// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_new_region() {
    build_and_run_test!(func, new_region_tester_1, new_region);
    build_and_run_test!(func, new_region_tester_2, new_region);
    build_and_run_test!(func, new_region_tester_3, new_region);
}
#[no_mangle]
fn new_region() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) regionref_t = mu_regionref);

    funcsig!((vm) sig = (int64) -> (regionref_t));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> size);
    ssa!((vm, func_v1) <regionref_t> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        res = NEWREGION size
    );

    inst!((vm, func_v1) blk_entry_print_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(size) {
        blk_entry_new_region,
        blk_entry_print_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  new_region_tester_1,
        TESTEE  func,
        INPUTS  int64(1),
        EXPECTED    NE  regionref_t(0),
    );

    emit_test!((vm)
        TESTER  new_region_tester_2,
        TESTEE  func,
        INPUTS  int64(1023),
        EXPECTED    NE  regionref_t(0),
    );

    emit_test!((vm)
        TESTER  new_region_tester_3,
        TESTEE  func,
        INPUTS  int64(64*1024 - 3),
        EXPECTED    NE  regionref_t(0),
    );

    vm
}

#[test]
fn test_rt_delete_region() {
    build_and_run_test!(func, delete_region_tester_1, delete_region);
}

fn delete_region() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) regionref_t = mu_regionref);

    funcsig!((vm) sig = (int64) -> ());
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> size);
    ssa!((vm, func_v1) <regionref_t> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        res = NEWREGION size
    );

    inst!((vm, func_v1) blk_entry_print_addr:
        PRINTHEX res
    );

    inst_rt!((vm, func_v1) blk_entry_delete_region:
        DELETEREGION    res
    );

    inst!((vm, func_v1) blk_entry_exit:
        THREADEXIT
    );

    define_block!((vm, func_v1) blk_entry(size) {
        blk_entry_new_region,
        blk_entry_print_addr,
        blk_entry_delete_region,
        blk_entry_exit
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  delete_region_tester_1,
        TESTEE  func,
        INPUTS  int64(1021),
    );

    vm
}

#[test]
fn test_rt_ralloc_untraced() {
    build_and_run_test!(func, ralloc_tester_1, ralloc_untraced);
    build_and_run_test!(func, ralloc_tester_2, ralloc_untraced);
}

fn ralloc_untraced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) array_int64_100 = mu_array(int64, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_int64_100));
    typedef!((vm) regionref_t = mu_regionref);

    funcsig!((vm) sig = (int64) -> (array_i100_ptr));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> reg_size);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_array:
        res = RALLOC    regref,     array_int64_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(reg_size) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_array,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(79),
        EXPECTED    EQ  array_i100_ptr(0),
    );

    emit_test!((vm)
        TESTER  ralloc_tester_2,
        TESTEE  func,
        INPUTS  int64(81),
        EXPECTED    NE  array_i100_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ralloc_hybrid_untraced() {
    build_and_run_test!(func, ralloc_tester_2, ralloc_hybrid_untraced);
    build_and_run_test!(func, ralloc_tester_1, ralloc_hybrid_untraced);
}

fn ralloc_hybrid_untraced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(int64, int64)(int64));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));
    typedef!    ((vm) regionref_t = mu_regionref);

    constdef!   ((vm) <int64> reg_size = Constant::Int(80));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    consta!((vm, func_v1) reg_size_local = reg_size);
    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size_local
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_hybrid:
        res = RALLOCHYBRID    regref, hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ralloc_tester_2,
        TESTEE  func,
        INPUTS  int64(9),
        EXPECTED    EQ  hybrid_int64_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ralloc_traced_array() {
    build_and_run_test!(func, ralloc_tester_1, ralloc_traced_array);
    build_and_run_test!(func, ralloc_tester_2, ralloc_traced_array);
}

fn ralloc_traced_array() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) i64_iref = mu_iref(int64));
    typedef!((vm) array_iref_100 = mu_array(i64_iref, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_iref_100));
    typedef!((vm) regionref_t = mu_regionref);

    funcsig!((vm) sig = (int64) -> (array_i100_ptr));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> reg_size);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_array:
        res = RALLOC    regref,     array_iref_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(reg_size) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_array,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(79),
        EXPECTED    EQ  array_i100_ptr(0),
    );

    emit_test!((vm)
        TESTER  ralloc_tester_2,
        TESTEE  func,
        INPUTS  int64(81),
        EXPECTED    NE  array_i100_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ralloc_traced_nested_struct() {
    build_and_run_test!(func, ralloc_tester_1, ralloc_traced_nested_struct);
}

fn ralloc_traced_nested_struct() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) i64_iref = mu_iref(int64));
    typedef!((vm) inner_struct = mu_struct(int64, i64_iref, int64));
    typedef!((vm) outer_struct = mu_struct(int64, inner_struct, int64, i64_iref));
    typedef!((vm) outer_struct_ptr = mu_uptr(outer_struct));
    typedef!((vm) regionref_t = mu_regionref);

    funcsig!((vm) sig = (int64) -> (outer_struct_ptr));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> reg_size);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <outer_struct_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_array:
        res = RALLOC    regref,     outer_struct
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(reg_size) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_array,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(79),
        EXPECTED    NE  outer_struct_ptr(0),
    );

    //    emit_test!((vm)
    //        TESTER  ralloc_tester_2,
    //        TESTEE  func,
    //        INPUTS  int64(81),
    //        EXPECTED    NE  array_i100_ptr(0),
    //    );

    vm
}

#[test]
fn test_rt_ralloc_hybrid_traced() {
    build_and_run_test!(func, ralloc_tester_1, ralloc_hybrid_fix_traced);
    build_and_run_test!(func, ralloc_tester_2, ralloc_hybrid_fix_traced);
    build_and_run_test!(func, ralloc_tester_1, ralloc_hybrid_var_traced);
    build_and_run_test!(func, ralloc_tester_2, ralloc_hybrid_var_traced);
}

fn ralloc_hybrid_fix_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) i64_iref = mu_iref(int64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(int64, i64_iref)(int64));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));
    typedef!    ((vm) regionref_t = mu_regionref);

    constdef!   ((vm) <int64> reg_size = Constant::Int(80));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    consta!((vm, func_v1) reg_size_local = reg_size);
    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size_local
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_hybrid:
        res = RALLOCHYBRID    regref, hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ralloc_tester_2,
        TESTEE  func,
        INPUTS  int64(9),
        EXPECTED    EQ  hybrid_int64_ptr(0),
    );

    vm
}

fn ralloc_hybrid_var_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) i64_iref = mu_iref(int64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(int64, int64)(i64_iref));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));
    typedef!    ((vm) regionref_t = mu_regionref);

    constdef!   ((vm) <int64> reg_size = Constant::Int(80));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    consta!((vm, func_v1) reg_size_local = reg_size);
    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <regionref_t> regref);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_new_region:
        regref = NEWREGION reg_size_local
    );

    inst!((vm, func_v1) blk_entry_print_reg_addr:
        PRINTHEX regref
    );

    inst_rt!((vm, func_v1) blk_entry_ralloc_hybrid:
        res = RALLOCHYBRID    regref, hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_new_region,
        blk_entry_print_reg_addr,
        blk_entry_ralloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ralloc_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ralloc_tester_2,
        TESTEE  func,
        INPUTS  int64(9),
        EXPECTED    EQ  hybrid_int64_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ealloc_untraced() {
    build_and_run_test!(func, ealloc_tester_1, e_alloc_untraced);
}

fn e_alloc_untraced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) array_i64_100 = mu_array(int64, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_i64_100));

    funcsig!((vm) sig = () -> (array_i100_ptr));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_array:
        res = EALLOC    array_i64_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_ealloc_array,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ealloc_tester_1,
        TESTEE  func,
        EXPECTED    NE  array_i100_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ealloc_hybrid_untraced() {
    build_and_run_test!(func, ealloc_tester_1, ealloc_hybrid_untraced);
    build_and_run_test!(func, ealloc_tester_2, ealloc_hybrid_untraced);
}

fn ealloc_hybrid_untraced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(int64, int64)(int64));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_hybrid:
        res = EALLOCHYBRID    hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_ealloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ealloc_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ealloc_tester_2,
        TESTEE  func,
        INPUTS  int64(80),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    vm
}

#[test]
fn test_rt_edelete_untraced() {
    build_and_run_test!(func, edelete_tester_1, e_delete_untraced);
}

fn e_delete_untraced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) array_i64_100 = mu_array(int64, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_i64_100));

    funcsig!((vm) sig = () -> ());
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_array:
        res = EALLOC    array_i64_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst_rt!((vm, func_v1) blk_entry_edelete_array:
        EDELETE    res
    );

    inst!((vm, func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_ealloc_array,
        blk_entry_print_blk_addr,
        blk_entry_edelete_array,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  edelete_tester_1,
        TESTEE  func,
    );

    vm
}

#[test]
fn test_rt_ealloc_traced() {
    build_and_run_test!(func, ealloc_tester_1, e_alloc_traced);
}

fn e_alloc_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) i64_iref = mu_iref(int64));
    typedef!((vm) array_iref_100 = mu_array(i64_iref, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_iref_100));

    funcsig!((vm) sig = () -> (array_i100_ptr));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_array:
        res = EALLOC    array_iref_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_ealloc_array,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ealloc_tester_1,
        TESTEE  func,
        EXPECTED    NE  array_i100_ptr(0),
    );

    vm
}

#[test]
fn test_rt_ealloc_hybrid_traced() {
    build_and_run_test!(func, ealloc_tester_1, ealloc_hybrid_fixed_traced);
    build_and_run_test!(func, ealloc_tester_2, ealloc_hybrid_fixed_traced);
    build_and_run_test!(func, ealloc_tester_3, ealloc_hybrid_var_traced);
    build_and_run_test!(func, ealloc_tester_4, ealloc_hybrid_var_traced);
}

fn ealloc_hybrid_fixed_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) i64_iref = mu_iref(int64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(int64, i64_iref)(int64));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_hybrid:
        res = EALLOCHYBRID    hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_ealloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ealloc_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ealloc_tester_2,
        TESTEE  func,
        INPUTS  int64(80),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    vm
}

fn ealloc_hybrid_var_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) i64_iref = mu_iref(int64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(i64_iref, int64)(i64_iref));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));

    funcsig!    ((vm) sig = (int64) -> (hybrid_int64_ptr));
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_hybrid:
        res = EALLOCHYBRID    hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_ealloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  ealloc_tester_3,
        TESTEE  func,
        INPUTS  int64(8),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    emit_test!((vm)
        TESTER  ealloc_tester_4,
        TESTEE  func,
        INPUTS  int64(80),
        EXPECTED    NE  hybrid_int64_ptr(0),
    );

    vm
}

#[test]
fn test_rt_edelete_traced() {
    build_and_run_test!(func, edelete_tester_1, e_delete_traced);
}

fn e_delete_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) i64_iref = mu_iref(int64));
    typedef!((vm) array_iref_100 = mu_array(i64_iref, 10));
    typedef!((vm) array_i100_ptr = mu_uptr(array_iref_100));

    funcsig!((vm) sig = () -> ());
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <array_i100_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_array:
        res = EALLOC    array_iref_100
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst_rt!((vm, func_v1) blk_entry_edelete_array:
        EDELETE    res
    );

    inst!((vm, func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_ealloc_array,
        blk_entry_print_blk_addr,
        blk_entry_edelete_array,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  edelete_tester_1,
        TESTEE  func,
    );

    vm
}

#[test]
fn test_rt_edelete_hybrid_traced() {
    build_and_run_test!(func, edelete_tester_1, edelete_hybrid_traced);
    build_and_run_test!(func, edelete_tester_2, edelete_hybrid_traced);
}

fn edelete_hybrid_traced() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) i64_iref = mu_iref(int64));
    typedef!    ((vm) hybrid_int64 = mu_hybrid(i64_iref, int64)(i64_iref));
    typedef!    ((vm) hybrid_int64_ptr = mu_uptr(hybrid_int64));

    funcsig!    ((vm) sig = (int64) -> ());
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int64> var_len);
    ssa!((vm, func_v1) <hybrid_int64_ptr> res);

    inst_rt!((vm, func_v1) blk_entry_ealloc_hybrid:
        res = EALLOCHYBRID    hybrid_int64, var_len
    );

    inst!((vm, func_v1) blk_entry_print_blk_addr:
        PRINTHEX res
    );

    inst_rt!((vm, func_v1) blk_entry_edelete_hybrid:
        EDELETE    res
    );

    inst!((vm, func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, func_v1) blk_entry(var_len) {
        blk_entry_ealloc_hybrid,
        blk_entry_print_blk_addr,
        blk_entry_edelete_hybrid,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  edelete_tester_1,
        TESTEE  func,
        INPUTS  int64(8),
    );

    emit_test!((vm)
        TESTER  edelete_tester_2,
        TESTEE  func,
        INPUTS  int64(80),
    );

    vm
}
