// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

use self::mu::runtime::thread::MU_DEFAULT_PRIORITY;

#[test]
fn test_rt_self_get_priority() {
    VM::start_logging_trace();

    let vm = Arc::new(self_get_priority());

    let compiler = Compiler::new(CompilerPolicy::default(), &vm);

    let func_id = vm.id_of("self_get_priority_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    vm.set_primordial_thread(func_id, true, vec![]);
    backend::emit_context(&vm);

    let executable = aot::link_test_primordial(
        vec![Arc::new("self_get_priority_func".to_string())],
        "test_bin_self_get_priority",
        &vm
    );
    linkutils::exec_path(executable);
}

fn self_get_priority() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);

    constdef!((vm) <int64> int64_0 = Constant::Int(0));
    constdef!((vm) <int64> int64_1 = Constant::Int(1));
    constdef!((vm) <thread_ref> const_null_threadref = Constant::NullRef);

    funcsig!((vm) self_get_priority_sig = () -> ());
    funcdecl!((vm) <self_get_priority_sig> self_get_priority_func);
    funcdef!((vm) <self_get_priority_sig> self_get_priority_func VERSION self_get_priority_func_v1);

    ssa!((vm, self_get_priority_func_v1) <int64> res);
    consta!((vm, self_get_priority_func_v1) const_null_threadref_local = const_null_threadref);
    consta!((vm, self_get_priority_func_v1) int64_1_local = int64_1);

    // blk_entry
    block!((vm, self_get_priority_func_v1) blk_entry);
    block!((vm, self_get_priority_func_v1) blk_validate);

    inst_rt!((vm, self_get_priority_func_v1) blk_entry_self_priority_get:
        res = THREADGETPRIORITY const_null_threadref_local
    );

    inst!((vm, self_get_priority_func_v1) blk_entry_priority_print:
        PRINTHEX res
    );

    inst!((vm, self_get_priority_func_v1) blk_entry_validate:
        BRANCH blk_validate (res)
    );

    mu_test_assert!(
        (vm, self_get_priority_func_v1, blk_validate)
            res     EQ  int64_1_local
    );

    define_block!((vm, self_get_priority_func_v1) blk_entry() {
        blk_entry_self_priority_get,
        blk_entry_priority_print,
        blk_entry_validate
    });

    define_func_ver!((vm) self_get_priority_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_validate
    });

    vm
}

#[test]
fn test_rt_self_set_priority() {
    VM::start_logging_trace();

    let vm = Arc::new(self_set_priority());

    let compiler = Compiler::new(CompilerPolicy::default(), &vm);

    let func_id = vm.id_of("self_set_priority_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    vm.set_primordial_thread(func_id, true, vec![]);
    backend::emit_context(&vm);

    let executable = aot::link_test_primordial(
        vec![Arc::new("self_set_priority_func".to_string())],
        "test_bin_self_set_priority",
        &vm
    );
    linkutils::exec_path(executable);
}

fn self_set_priority() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);

    constdef!((vm) <int64> int64_0 = Constant::Int(0));
    constdef!((vm) <int64> int64_17 = Constant::Int(17));
    constdef!((vm) <thread_ref> const_null_threadref = Constant::NullRef);

    funcsig!((vm) self_set_priority_sig = () -> ());
    funcdecl!((vm) <self_set_priority_sig> self_set_priority_func);
    funcdef!((vm) <self_set_priority_sig> self_set_priority_func VERSION self_set_priority_func_v1);

    ssa!((vm, self_set_priority_func_v1) <int64> res);
    consta!((vm, self_set_priority_func_v1) const_null_threadref_local = const_null_threadref);
    consta!((vm, self_set_priority_func_v1) int64_17_local = int64_17);

    // blk_entry
    block!((vm, self_set_priority_func_v1) blk_entry);
    block!((vm, self_set_priority_func_v1) blk_validate);

    inst_rt!((vm, self_set_priority_func_v1) blk_entry_self_priority_set:
        THREADSETPRIORITY const_null_threadref_local,  int64_17_local
    );

    inst_rt!((vm, self_set_priority_func_v1) blk_entry_self_priority_get:
        res = THREADGETPRIORITY const_null_threadref_local
    );

    inst!((vm, self_set_priority_func_v1) blk_entry_priority_print:
        PRINTHEX res
    );

    inst!((vm, self_set_priority_func_v1) blk_entry_validate:
        BRANCH blk_validate (res)
    );

    mu_test_assert!(
        (vm, self_set_priority_func_v1, blk_validate)
            res     EQ  int64_17_local
    );

    define_block!((vm, self_set_priority_func_v1) blk_entry() {
        blk_entry_self_priority_set,
        blk_entry_self_priority_get,
        blk_entry_priority_print,
        blk_entry_validate
    });

    define_func_ver!((vm) self_set_priority_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_validate
    });

    vm
}

#[test]
fn test_rt_self_isset_affinity() {
    VM::start_logging_trace();

    let test_cases = [[0, 1], [1, 0], [2, 0]];

    for i in 0..3 {
        let vm =
            Arc::new(self_isset_affinity(test_cases[i][0], test_cases[i][1]));

        let compiler = Compiler::new(CompilerPolicy::default(), &vm);

        let func_id = vm.id_of("test_func");
        {
            let funcs = vm.funcs().read().unwrap();
            let func = funcs.get(&func_id).unwrap().read().unwrap();
            let func_vers = vm.func_vers().read().unwrap();
            let mut func_ver = func_vers
                .get(&func.cur_ver.unwrap())
                .unwrap()
                .write()
                .unwrap();

            compiler.compile(&mut func_ver);
        }

        vm.set_primordial_thread(func_id, true, vec![]);
        backend::emit_context(&vm);

        let executable = aot::link_test_primordial(
            vec![Arc::new("test_func".to_string())],
            "test_bin_self_isset_affinity",
            &vm
        );
        linkutils::exec_path(executable);
    }
}

fn self_isset_affinity(cpu_num: u64, expected: u64) -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int1  = mu_int(1));
    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);

    constdef!((vm) <int64> cpu_id = Constant::Int(cpu_num));
    constdef!((vm) <int64> exp_res = Constant::Int(expected));
    constdef!((vm) <thread_ref> null_threadref = Constant::NullRef);

    funcsig!((vm) test_sig = () -> ());
    funcdecl!((vm) <test_sig> test_func);
    funcdef!((vm) <test_sig> test_func VERSION test_func_v1);

    ssa!((vm, test_func_v1) <int1> res);
    consta!((vm, test_func_v1) null_threadref_local = null_threadref);
    consta!((vm, test_func_v1) cpu_id_local = cpu_id);
    consta!((vm, test_func_v1) exp_res_local = exp_res);

    // blk_entry
    block!((vm, test_func_v1) blk_entry);
    block!((vm, test_func_v1) blk_validate);

    inst_rt!((vm, test_func_v1) blk_entry_inst_1:
        res = THREADISSETCPU null_threadref_local, cpu_id_local
    );

    inst!((vm, test_func_v1) blk_entry_print:
        PRINTBOOL res
    );

    inst!((vm, test_func_v1) blk_entry_validate:
        BRANCH blk_validate (res)
    );

    mu_test_assert!(
        (vm, test_func_v1, blk_validate)
            res     EQ  exp_res_local
    );

    define_block!((vm, test_func_v1) blk_entry() {
        blk_entry_inst_1,
        blk_entry_print,
        blk_entry_validate
    });

    define_func_ver!((vm) test_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_validate
    });

    vm
}

#[test]
fn test_rt_self_set_cpu() {
    VM::start_logging_trace();

    let test_cases = [
        [1, 0, 1],
        [1, 1, 1],
        [1, 2, 0],
        [2, 0, 1],
        [2, 1, 0],
        [2, 2, 1]
    ];

    for i in 0..test_cases.len() {
        let vm = Arc::new(self_set_affinity(
            test_cases[i][0],
            test_cases[i][1],
            test_cases[i][2]
        ));

        let compiler = Compiler::new(CompilerPolicy::default(), &vm);

        let func_id = vm.id_of("test_func");
        {
            let funcs = vm.funcs().read().unwrap();
            let func = funcs.get(&func_id).unwrap().read().unwrap();
            let func_vers = vm.func_vers().read().unwrap();
            let mut func_ver = func_vers
                .get(&func.cur_ver.unwrap())
                .unwrap()
                .write()
                .unwrap();

            compiler.compile(&mut func_ver);
        }

        vm.set_primordial_thread(func_id, true, vec![]);
        backend::emit_context(&vm);

        let executable = aot::link_test_primordial(
            vec![Arc::new("test_func".to_string())],
            "test_bin_self_set_affinity",
            &vm
        );
        linkutils::exec_path(executable);
    }
}

fn self_set_affinity(cpu_to_set: u64, cpu_to_check: u64, expected: u64) -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int1  = mu_int(1));
    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);

    constdef!((vm) <int64> _cpu_to_set = Constant::Int(cpu_to_set));
    constdef!((vm) <int64> _cpu_to_check = Constant::Int(cpu_to_check));
    constdef!((vm) <int64> exp_res = Constant::Int(expected));
    constdef!((vm) <thread_ref> null_threadref = Constant::NullRef);

    funcsig!((vm) test_sig = () -> ());
    funcdecl!((vm) <test_sig> test_func);
    funcdef!((vm) <test_sig> test_func VERSION test_func_v1);

    ssa!((vm, test_func_v1) <int1> res);
    consta!((vm, test_func_v1) null_threadref_local = null_threadref);
    consta!((vm, test_func_v1) _cpu_to_set_local = _cpu_to_set);
    consta!((vm, test_func_v1) _cpu_to_check_local = _cpu_to_check);
    consta!((vm, test_func_v1) exp_res_local = exp_res);

    // blk_entry
    block!((vm, test_func_v1) blk_entry);
    block!((vm, test_func_v1) blk_validate);

    inst_rt!((vm, test_func_v1) blk_entry_inst_1:
        THREADSETCPU null_threadref_local, _cpu_to_set_local
    );

    inst_rt!((vm, test_func_v1) blk_entry_inst_2:
        res = THREADISSETCPU null_threadref_local, _cpu_to_check_local
    );

    inst!((vm, test_func_v1) blk_entry_print:
        PRINTBOOL res
    );

    inst!((vm, test_func_v1) blk_entry_validate:
        BRANCH blk_validate (res)
    );

    mu_test_assert!(
        (vm, test_func_v1, blk_validate)
            res     EQ  exp_res_local
    );

    define_block!((vm, test_func_v1) blk_entry() {
        blk_entry_inst_1,
        blk_entry_inst_2,
        blk_entry_print,
        blk_entry_validate
    });

    define_func_ver!((vm) test_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_validate
    });

    vm
}

#[test]
fn test_rt_self_clear_cpu() {
    VM::start_logging_trace();

    let test_cases = [[1, 0, 0, 0], [1, 0, 1, 1], [1, 1, 0, 1], [1, 1, 1, 0]];

    for i in 0..test_cases.len() {
        let vm = Arc::new(self_clear_affinity(
            test_cases[i][0],
            test_cases[i][1],
            test_cases[i][2],
            test_cases[i][3]
        ));

        let compiler = Compiler::new(CompilerPolicy::default(), &vm);

        let func_id = vm.id_of("test_func");
        {
            let funcs = vm.funcs().read().unwrap();
            let func = funcs.get(&func_id).unwrap().read().unwrap();
            let func_vers = vm.func_vers().read().unwrap();
            let mut func_ver = func_vers
                .get(&func.cur_ver.unwrap())
                .unwrap()
                .write()
                .unwrap();

            compiler.compile(&mut func_ver);
        }

        vm.set_primordial_thread(func_id, true, vec![]);
        backend::emit_context(&vm);

        let executable = aot::link_test_primordial(
            vec![Arc::new("test_func".to_string())],
            "test_bin_self_clear_affinity",
            &vm
        );
        linkutils::exec_path(executable);
    }
}

fn self_clear_affinity(
    cpu_to_set: u64,
    cpu_to_clr: u64,
    cpu_to_check: u64,
    expected: u64
) -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int1  = mu_int(1));
    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);

    constdef!((vm) <int64> _cpu_to_set = Constant::Int(cpu_to_set));
    constdef!((vm) <int64> _cpu_to_clr = Constant::Int(cpu_to_clr));
    constdef!((vm) <int64> _cpu_to_check = Constant::Int(cpu_to_check));
    constdef!((vm) <int64> exp_res = Constant::Int(expected));
    constdef!((vm) <thread_ref> null_threadref = Constant::NullRef);

    funcsig!((vm) test_sig = () -> ());
    funcdecl!((vm) <test_sig> test_func);
    funcdef!((vm) <test_sig> test_func VERSION test_func_v1);

    ssa!((vm, test_func_v1) <int1> res);
    consta!((vm, test_func_v1) null_threadref_local = null_threadref);
    consta!((vm, test_func_v1) _cpu_to_set_local = _cpu_to_set);
    consta!((vm, test_func_v1) _cpu_to_clr_local = _cpu_to_clr);
    consta!((vm, test_func_v1) _cpu_to_check_local = _cpu_to_check);
    consta!((vm, test_func_v1) exp_res_local = exp_res);

    // blk_entry
    block!((vm, test_func_v1) blk_entry);
    block!((vm, test_func_v1) blk_validate);

    inst_rt!((vm, test_func_v1) blk_entry_inst_1:
        THREADSETCPU null_threadref_local, _cpu_to_set_local
    );

    inst_rt!((vm, test_func_v1) blk_entry_inst_2:
        THREADCLEARCPU null_threadref_local, _cpu_to_clr_local
    );

    inst_rt!((vm, test_func_v1) blk_entry_inst_3:
        res = THREADISSETCPU null_threadref_local, _cpu_to_check_local
    );

    inst!((vm, test_func_v1) blk_entry_print:
        PRINTBOOL res
    );

    inst!((vm, test_func_v1) blk_entry_validate:
        BRANCH blk_validate (res)
    );

    mu_test_assert!(
        (vm, test_func_v1, blk_validate)
            res     EQ  exp_res_local
    );

    define_block!((vm, test_func_v1) blk_entry() {
        blk_entry_inst_1,
        blk_entry_inst_2,
        blk_entry_inst_3,
        blk_entry_print,
        blk_entry_validate
    });

    define_func_ver!((vm) test_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_validate
    });

    vm
}

#[test]
pub fn test_rt_self_get_attr() {
    build_and_run_test!(func, get_attr_tester_1, self_get_attr);
}

fn self_get_attr() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int1  = mu_int(1));
    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);
    typedef!((vm) attr_ref = mu_attrref);

    constdef!((vm) <int64> int64_0 = Constant::Int(0));
    constdef!((vm) <int64> int64_1 = Constant::Int(1));
    constdef!((vm) <int1> int1_false = Constant::Int(0));
    constdef!((vm) <int1> int1_true = Constant::Int(1));
    constdef!((vm) <thread_ref> null_threadref = Constant::NullRef);

    funcsig!((vm) sig = () -> (int64));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    ssa!((vm, func_v1) <attr_ref> attr);
    ssa!((vm, func_v1) <int64> priority);
    ssa!((vm, func_v1) <int1> cpu0_val);
    ssa!((vm, func_v1) <int1> cpu1_val);
    consta!((vm, func_v1) null_threadref_local = null_threadref);
    consta!((vm, func_v1) int64_0_local = int64_0);
    consta!((vm, func_v1) int64_1_local = int64_1);
    consta!((vm, func_v1) false_local = int1_false);
    consta!((vm, func_v1) true_local = int1_true);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    inst_rt!((vm, func_v1) blk_entry_get_attr:
        attr = THREADGETATTR null_threadref_local
    );
    inst!((vm, func_v1) blk_entry_print_attr:
        PRINTHEX attr
    );
    inst_rt!((vm, func_v1) blk_entry_get_priority:
        priority = ATTRGETPRIORITY attr
    );
    inst!((vm, func_v1) blk_entry_print_priority:
        PRINTHEX priority
    );
    inst_rt!((vm, func_v1) blk_entry_get_cpu_0:
        cpu0_val = ATTRISSETCPU attr, int64_0_local
    );
    inst!((vm, func_v1) blk_entry_print_cpu_0_0:
        PRINTBOOL cpu0_val
    );
    inst!((vm, func_v1) blk_entry_print_cpu_0_1:
        PRINTBOOL cpu0_val
    );
    inst_rt!((vm, func_v1) blk_entry_get_cpu_1:
        cpu1_val = ATTRISSETCPU attr, int64_1_local
    );
    inst!((vm, func_v1) blk_entry_print_cpu_1_0:
        PRINTBOOL cpu1_val
    );
    inst!((vm, func_v1) blk_entry_print_cpu_1_1:
        PRINTBOOL cpu1_val
    );
    inst!((vm, func_v1) blk_entry_exit:
        RET (priority)
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_get_attr,
        blk_entry_print_attr,
        blk_entry_get_priority,
        blk_entry_print_priority,
        blk_entry_get_cpu_0,
        blk_entry_print_cpu_0_0,
        blk_entry_print_cpu_0_1,
        blk_entry_get_cpu_1,
        blk_entry_print_cpu_1_0,
        blk_entry_print_cpu_1_1,
        blk_entry_exit
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  get_attr_tester_1,
        TESTEE  func,
        EXPECTED    EQ  int64(MU_DEFAULT_PRIORITY),
    );

    vm
}

#[test]
pub fn test_rt_self_set_attr() {
    build_and_run_test!(func, set_attr_tester_1, self_set_attr);
}

fn self_set_attr() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int1  = mu_int(1));
    typedef!((vm) int64  = mu_int(64));
    typedef!((vm) thread_ref = mu_threadref);
    typedef!((vm) attr_ref = mu_attrref);

    constdef!((vm) <int64> int64_0 = Constant::Int(0));
    constdef!((vm) <int64> int64_1 = Constant::Int(1));
    constdef!((vm) <int64> int64_2 = Constant::Int(2));
    constdef!((vm) <int1> int1_false = Constant::Int(0));
    constdef!((vm) <int1> int1_true = Constant::Int(1));
    constdef!((vm) <thread_ref> null_threadref = Constant::NullRef);

    funcsig!((vm) sig = () -> (int64));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    ssa!((vm, func_v1) <attr_ref> attr_to_set);
    ssa!((vm, func_v1) <attr_ref> attr_to_get);
    ssa!((vm, func_v1) <int64> priority);
    ssa!((vm, func_v1) <int1> cpu0_val);
    ssa!((vm, func_v1) <int1> cpu1_val);
    consta!((vm, func_v1) null_threadref_local = null_threadref);
    consta!((vm, func_v1) int64_0_local = int64_0);
    consta!((vm, func_v1) int64_1_local = int64_1);
    consta!((vm, func_v1) int64_2_local = int64_2);
    consta!((vm, func_v1) false_local = int1_false);
    consta!((vm, func_v1) true_local = int1_true);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    inst_rt!((vm, func_v1) blk_entry_new_attr:
        attr_to_set = NEWATTR
    );
    inst!((vm, func_v1) blk_entry_print_attr_to_set:
        PRINTHEX attr_to_set
    );
    inst_rt!((vm, func_v1) blk_entry_attr_set_priority:
        ATTRSETPRIORITY attr_to_set, int64_2_local
    );
    inst_rt!((vm, func_v1) blk_entry_attr_clear_cpu_0:
        ATTRCLEARCPU attr_to_set, int64_0_local
    );
    inst_rt!((vm, func_v1) blk_entry_attr_set_cpu_1:
        ATTRSETCPU attr_to_set, int64_1_local
    );
    inst_rt!((vm, func_v1) blk_entry_thread_set_attr:
        THREADSETATTR null_threadref_local, attr_to_set
    );

    inst_rt!((vm, func_v1) blk_entry_thread_get_attr:
        attr_to_get = THREADGETATTR null_threadref_local
    );
    inst!((vm, func_v1) blk_entry_print_attr_to_get:
        PRINTHEX attr_to_get
    );

    inst_rt!((vm, func_v1) blk_entry_get_priority:
        priority = ATTRGETPRIORITY attr_to_get
    );
    inst!((vm, func_v1) blk_entry_print_priority:
        PRINTHEX priority
    );
    inst_rt!((vm, func_v1) blk_entry_attr_get_cpu_0:
        cpu0_val = ATTRISSETCPU attr_to_get, int64_0_local
    );
    inst!((vm, func_v1) blk_entry_print_cpu_0_0:
        PRINTBOOL cpu0_val
    );
    inst!((vm, func_v1) blk_entry_print_cpu_0_1:
        PRINTBOOL cpu0_val
    );
    inst_rt!((vm, func_v1) blk_entry_attr_get_cpu_1:
        cpu1_val = ATTRISSETCPU attr_to_get, int64_1_local
    );
    inst!((vm, func_v1) blk_entry_print_cpu_1_0:
        PRINTBOOL cpu1_val
    );
    inst!((vm, func_v1) blk_entry_print_cpu_1_1:
        PRINTBOOL cpu1_val
    );
    inst!((vm, func_v1) blk_entry_exit:
        RET (priority)
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_new_attr,
        blk_entry_print_attr_to_set,
        blk_entry_attr_set_priority,
        blk_entry_attr_clear_cpu_0,
        blk_entry_attr_set_cpu_1,
        blk_entry_thread_set_attr,
        blk_entry_thread_get_attr,
        blk_entry_print_attr_to_get,
        blk_entry_get_priority,
        blk_entry_print_priority,
        blk_entry_attr_get_cpu_0,
        blk_entry_print_cpu_0_0,
        blk_entry_print_cpu_0_1,
        blk_entry_attr_get_cpu_1,
        blk_entry_print_cpu_1_0,
        blk_entry_print_cpu_1_1,
        blk_entry_exit
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  set_attr_tester_1,
        TESTEE  func,
        EXPECTED    EQ  int64(2),
    );

    vm
}
