// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![allow(unused_imports)]

extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::linkutils;
use self::mu::linkutils::aot;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use std::sync::Arc;
use std::sync::RwLock;

#[test]
fn test_thread_create() {
    VM::start_logging_trace();

    let vm = Arc::new(primordial_main());

    let compiler = Compiler::new(CompilerPolicy::default(), &vm);

    let func_id = vm.id_of("primordial_main");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    vm.set_primordial_thread(func_id, true, vec![]);
    backend::emit_context(&vm);

    let executable = aot::link_primordial(
        vec![Arc::new("primordial_main".to_string())],
        "primordial_main_test",
        &vm
    );
    linkutils::exec_path(executable);
}

fn primordial_main() -> VM {
    let vm = VM::new();

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> primordial_main);
    funcdef!    ((vm) <sig> primordial_main VERSION primordial_main_v1);

    block!      ((vm, primordial_main_v1) blk_entry);
    inst!       ((vm, primordial_main_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!((vm, primordial_main_v1) blk_entry() {
        blk_entry_threadexit
    });

    define_func_ver!((vm) primordial_main_v1(entry: blk_entry) {
        blk_entry
    });

    vm
}

#[test]
fn test_main_with_retval() {
    let vm = Arc::new(main_with_retval());

    let func_id = vm.id_of("main_with_retval");
    let func_handle = vm.handle_from_func(func_id);
    vm.make_boot_image(
        vec![func_id],
        Some(&func_handle),
        None,
        None,
        vec![],
        vec![],
        vec![],
        vec![],
        "test_main_with_retval".to_string()
    );

    // run
    let executable = {
        use std::path;
        let mut path = path::PathBuf::new();
        path.push(&vm.vm_options.flag_aot_emit_dir);
        path.push("test_main_with_retval");
        path
    };
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    println!("return code: {}", ret_code);
    assert!(ret_code == 42);
}

fn main_with_retval() -> VM {
    let vm = VM::new();

    typedef!    ((vm) int32 = mu_int(32));
    constdef!   ((vm) <int32> int32_42 = Constant::Int(42));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> main_with_retval);
    funcdef!    ((vm) <sig> main_with_retval VERSION main_with_retval_v1);

    block!      ((vm, main_with_retval_v1) blk_entry);

    consta!     ((vm, main_with_retval_v1) int32_42_local = int32_42);
    inst!       ((vm, main_with_retval_v1) blk_entry_set_retval:
        SET_RETVAL int32_42_local
    );

    inst!       ((vm, main_with_retval_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!((vm, main_with_retval_v1) blk_entry() {
        blk_entry_set_retval,
        blk_entry_threadexit
    });

    define_func_ver!((vm) main_with_retval_v1(entry: blk_entry) {
        blk_entry
    });

    vm
}

#[test]
fn test_new_thread() {
    VM::start_logging_trace();

    let vm = Arc::new(create_thread());

    let compiler = Compiler::new(CompilerPolicy::default(), &vm);

    let func_id = vm.id_of("the_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    let func_id = vm.id_of("create_thread_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    vm.set_primordial_thread(func_id, true, vec![]);
    backend::emit_context(&vm);

    let executable = aot::link_primordial(
        vec![
            Arc::new("create_thread_func".to_string()),
            Arc::new("the_func".to_string()),
        ],
        "create_thread_func_test",
        &vm
    );
    linkutils::exec_path(executable);
}

//fn empty_function() -> VM {
//    let vm = VM::new_with_opts("--aot-link-static");
//
//    typedef!    ((vm) int64  = mu_int(64));
//
//    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));
//
//    funcsig!    ((vm) sig = () -> (int64));
//    funcdecl!   ((vm) <sig> the_func);
//    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);
//
//    block!      ((vm, the_func_v1) blk_entry);
//
//    inst!((vm, the_func_v1) blk_entry_ret:
//        RET (int64_0)
//    );
//
//    define_block!   ((vm, the_func_v1) blk_entry() {
//        blk_entry_ret
//    });
//
//    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
//        blk_entry
//    });
//
//    vm
//}

fn create_thread() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));

    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> the_func);
    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);

    consta!     ((vm, the_func_v1) int64_0_local = int64_0);

    block!      ((vm, the_func_v1) blk_entry);

    inst!((vm, the_func_v1) blk_entry_print:
        PRINTHEX int64_0_local
    );

    inst!((vm, the_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!   ((vm, the_func_v1) blk_entry() {
        blk_entry_print,
        blk_entry_ret
    });

    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
        blk_entry
    });

    typedef!    ((vm) func_ref = mu_funcref(sig));
    typedef!    ((vm) stack_ref = mu_stackref);
    typedef!    ((vm) thread_ref = mu_threadref);

    constdef!   ((vm) <func_ref> const_funcref = Constant::FuncRef(the_func));

    funcsig!    ((vm) create_thread_sig = () -> ());
    funcdecl!   ((vm) <create_thread_sig> create_thread_func);
    funcdef!    ((vm) <create_thread_sig> create_thread_func VERSION create_thread_func_v1);

    ssa!        ((vm, create_thread_func_v1) <stack_ref> the_stack_ref);
    ssa!        ((vm, create_thread_func_v1) <thread_ref> the_thread_ref);
    consta!     ((vm, create_thread_func_v1) const_funcref_local = const_funcref);

    // blk_entry
    block!      ((vm, create_thread_func_v1) blk_entry);

    inst!((vm, create_thread_func_v1) blk_entry_create_stack:
        the_stack_ref = NEWSTACK const_funcref_local
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_stack_print:
        PRINTHEX the_stack_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_thread:
        the_thread_ref = NEWTHREAD STACK: the_stack_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_thread_print:
        PRINTHEX the_thread_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, create_thread_func_v1) blk_entry() {
            blk_entry_create_stack,
            blk_entry_create_stack_print,
            blk_entry_create_thread,
            blk_entry_create_thread_print,
    //        blk_entry_start_thread,
            blk_entry_ret
        });

    define_func_ver!((vm) create_thread_func_v1 (entry: blk_entry) {
        blk_entry
    });

    vm
}
