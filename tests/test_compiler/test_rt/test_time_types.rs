// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_new_timeval() {
    build_and_run_test!(func, new_timeval_tester_1, new_timeval);
}

fn new_timeval() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) timeval_t         = mu_timeval);
    typedef!    ((vm) ref_timeval_t     = mu_ref(timeval_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    block!      ((vm, func_v1) blk_entry);

    ssa!        ((vm, func_v1) <ref_timeval_t> a);
    inst!       ((vm, func_v1) blk_entry_new1:
        a = NEW <timeval_t>
    );

    inst!       ((vm, func_v1) blk_entry_print1:
        PRINTHEX a
    );

    inst!       ((vm, func_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, func_v1) blk_entry() {
        blk_entry_new1,
        blk_entry_print1,
        blk_entry_threadexit
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {blk_entry});

    emit_test!((vm)
        TESTER  new_timeval_tester_1,
        TESTEE  func,
    );

    vm
}

#[test]
fn test_rt_new_timerref() {
    build_and_run_test!(func, new_timerref_tester_1, new_timerref);
}

fn new_timerref() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) timerref_t         = mu_timerref);
    typedef!    ((vm) ref_timerref_t     = mu_ref(timerref_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    block!      ((vm, func_v1) blk_entry);

    ssa!        ((vm, func_v1) <ref_timerref_t> a);
    inst!       ((vm, func_v1) blk_entry_new1:
        a = NEW <timerref_t>
    );

    inst!       ((vm, func_v1) blk_entry_print1:
        PRINTHEX a
    );

    inst!       ((vm, func_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, func_v1) blk_entry() {
        blk_entry_new1,
        blk_entry_print1,
        blk_entry_threadexit
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {blk_entry});

    emit_test!((vm)
        TESTER  new_timerref_tester_1,
        TESTEE  func,
    );

    vm
}
