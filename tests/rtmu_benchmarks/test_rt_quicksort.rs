// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::BinOp;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_quicksort() {
    build_and_run_test!(
        VM_BUILDER  rtmu_quicksort_ealloc(1000000),
        DEP_FUNCS   (init_func, cs_func, qs_func, part_func),
        TESTER_NAME qs_ealloc_tester_1,
    );
    build_and_run_test!(
        VM_BUILDER  rtmu_quicksort_ralloc(1000000),
        DEP_FUNCS   (init_func, cs_func, qs_func, part_func),
        TESTER_NAME qs_ralloc_tester_1,
    );
}

fn rtmu_quicksort_ealloc(__array_size: usize) -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int1             = mu_int    (1));
    typedef!    ((vm) int64             = mu_int    (64));
    typedef!    ((vm) int64_ptr         = mu_uptr   (int64));
    typedef!    ((vm) array_int64       = mu_array  (int64, __array_size));
    typedef!    ((vm) array_ptr         = mu_uptr   (array_int64));

    constdef!   ((vm) <int64> array_size    = Constant::Int((__array_size) as u64));
    constdef!   ((vm) <int64> int64_0       = Constant::Int(0));
    constdef!   ((vm) <int64> int64_1       = Constant::Int(1));
    constdef!   ((vm) <int64> int64_BIG       = Constant::Int(1000000));

    constdef!   ((vm) <int64> pos_1       = Constant::Int(474747));

    // ------------------------------------------------------------------
    // the quicksort partition function
    //
    //     fn part_func(array, low, high) -> partition_index
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) part_sig =    (array_ptr, int64, int64) -> (int64));
    funcdecl!   ((vm) <part_sig>    part_func);
    funcdef!    ((vm) <part_sig>    part_func VERSION part_func_v1);

    ssa!        ((vm, part_func_v1) <array_ptr>   array);
    ssa!        ((vm, part_func_v1) <int64>       low);
    ssa!        ((vm, part_func_v1) <int64>       high);

    ssa!        ((vm, part_func_v1) <int64_ptr>   pi_ptr);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_val);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_i_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_i_val);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_j_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_j_val);
    ssa!        ((vm, part_func_v1) <int64>       swap_temp);
    ssa!        ((vm, part_func_v1) <int64>       pi);
    ssa!        ((vm, part_func_v1) <int64>       ic); // pivot counter
    ssa!        ((vm, part_func_v1) <int64>       next_ic); // ic++
    ssa!        ((vm, part_func_v1) <int64>       jc); // tracer counter
    ssa!        ((vm, part_func_v1) <int64>       next_jc); // jc++
    ssa!        ((vm, part_func_v1) <int1>        cmp_res);

    block!      ((vm, part_func_v1) blk_entry);
    block!      ((vm, part_func_v1) blk_core);
    block!      ((vm, part_func_v1) blk_core_proc);
    block!      ((vm, part_func_v1) blk_core_proc_swap);
    block!      ((vm, part_func_v1) blk_core_proc_loop);
    block!      ((vm, part_func_v1) blk_exit);

    consta!     ((vm, part_func_v1) int64_1_local  = int64_1);

    /*
    BLOCK entry {
        pi = array[high]
        ic = low - 1
        BRANCH core_loop(pi, ic)
    }
    */
    inst!       ((vm, part_func_v1) blk_entry_get_pi_ptr:
        pi_ptr =  GETELEMIREF array high (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_entry_load_pi:
        pi = LOAD pi_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_entry_calc_ic:
        ic = BINOP (BinOp::Sub) low int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_entry_calc_jc:
        jc = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_entry_b_core:
        BRANCH      blk_core   (array, low, high, pi, ic, jc)
    );

    /*
    BLOCK core {
        if jcounter < high
            BRANCH core_proc
        else
            BRANCH exit
    }
    */
    inst!       ((vm, part_func_v1) blk_core_cmp:
        cmp_res = CMPOP (CmpOp::ULT) jc high
    );
    inst!       ((vm, part_func_v1) blk_core_branch2:
        BRANCH2 (cmp_res, array, low, high, pi, ic, jc)
            IF (OP 0)
            THEN blk_core_proc  (vec![1, 2, 3, 4, 5, 6]) WITH 0.9f32,
            ELSE blk_exit (vec![1, 3, 5])
    );

    /*
    BLOCK core_proc {
        if array[jc] <= pi
            BRANCH  core_proc_swap
        else
            BRANCH  core_proc_loop
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_elem_ptr:
        elem_ptr =  GETELEMIREF array jc (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_load_elem:
        elem_val = LOAD elem_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_cmp:
        cmp_res = CMPOP (CmpOp::ULE) elem_val pi
    );
    inst!       ((vm, part_func_v1) blk_core_proc_branch2:
        BRANCH2 (cmp_res, array, low, high, pi, ic, jc)
            IF (OP 0)
            THEN blk_core_proc_swap  (vec![1, 2, 3, 4, 5, 6]) WITH 0.9f32,
            ELSE blk_core_proc_loop (vec![1, 2, 3, 4, 5, 6])
    );

    /*
    BLOCK   core_proc_swap {
        ic++
        if array[jc] <= pivot
            swap_elements(i,j)
        BRANCH core_proc_loop
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_swap_ipp:
        next_ic = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_elem_i_ptr:
        elem_i_ptr =  GETELEMIREF array next_ic (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_load_elem_i:
        elem_i_val = LOAD elem_i_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_elem_j_ptr:
        elem_j_ptr =  GETELEMIREF array jc (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_load_elem_j:
        elem_j_val = LOAD elem_j_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_swap_elem_i:
        STORE elem_i_ptr elem_j_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_swap_elem_j:
        STORE elem_j_ptr elem_i_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_b_loop:
        BRANCH      blk_core_proc_loop   (array, low, high, pi, next_ic, jc)
    );

    /*
    BLOCK core_proc_loop {
        jc++
        BRANCH core
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_loop_jpp:
        next_jc = BINOP (BinOp::Add) jc int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_core_proc_loop_b_core:
        BRANCH      blk_core   (array, low, high, pi, ic, next_jc)
    );

    /*
    BLOCK exit {
        swap_elements(i+1, high)
        ret (i+1)
    }
    */
    inst!       ((vm, part_func_v1) blk_exit_ipp:
        next_ic = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_exit_elem_i_ptr:
        elem_i_ptr =  GETELEMIREF array next_ic (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_exit_load_elem_i:
        elem_i_val = LOAD elem_i_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_elem_high_ptr:
        elem_j_ptr =  GETELEMIREF array high (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_exit_load_elem_high:
        elem_j_val = LOAD elem_j_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_swap_elem_i:
        STORE elem_i_ptr elem_j_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_swap_elem_high:
        STORE elem_j_ptr elem_i_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_ret:
        RET (next_ic)
    );

    define_block!   ((vm, part_func_v1) blk_entry(array, low, high) {
        blk_entry_get_pi_ptr,
        blk_entry_load_pi,
        blk_entry_calc_ic,
        blk_entry_calc_jc,
        blk_entry_b_core
    });
    define_block!   ((vm, part_func_v1) blk_core(array, low, high, pi, ic, jc) {
        blk_core_cmp,
        blk_core_branch2
    });
    define_block!   ((vm, part_func_v1) blk_core_proc(array, low, high, pi, ic, jc) {
        blk_core_proc_elem_ptr,
        blk_core_proc_load_elem,
        blk_core_proc_cmp,
        blk_core_proc_branch2
    });
    define_block!   ((vm, part_func_v1) blk_core_proc_swap(array, low, high, pi, ic, jc) {
        blk_core_proc_swap_ipp,
        blk_core_proc_swap_elem_i_ptr,
        blk_core_proc_swap_load_elem_i,
        blk_core_proc_swap_elem_j_ptr,
        blk_core_proc_swap_load_elem_j,
        blk_core_proc_swap_swap_elem_i,
        blk_core_proc_swap_swap_elem_j,
        blk_core_proc_swap_b_loop
    });
    define_block!   ((vm, part_func_v1) blk_core_proc_loop(array, low, high, pi, ic, jc) {
        blk_core_proc_loop_jpp,
        blk_core_proc_loop_b_core
    });
    define_block!   ((vm, part_func_v1) blk_exit(array, high, ic) {
        blk_exit_ipp,
        blk_exit_elem_i_ptr,
        blk_exit_load_elem_i,
        blk_exit_elem_high_ptr,
        blk_exit_load_elem_high,
        blk_exit_swap_elem_i,
        blk_exit_swap_elem_high,
        blk_exit_ret
    });
    define_func_ver!((vm) part_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_core_proc,
        blk_core_proc_swap,
        blk_core_proc_loop,
        blk_exit
    });
    // ------------------------------------------------------------------

    typedef!    ((vm) part_sig_funcref = mu_funcref(part_sig));
    constdef!   ((vm) <part_sig_funcref> part_funcref = Constant::FuncRef(part_func));

    // ------------------------------------------------------------------
    // the basic quicksort function
    //
    //      fn qs_func(array, low, high)
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) qs_sig =    (array_ptr, int64, int64) -> ());
    funcdecl!   ((vm) <qs_sig>    qs_func);
    funcdef!    ((vm) <qs_sig>    qs_func VERSION qs_func_v1);

    typedef!    ((vm) qs_sig_funcref = mu_funcref(qs_sig));
    constdef!   ((vm) <qs_sig_funcref> qs_funcref = Constant::FuncRef(qs_func));

    block!      ((vm, qs_func_v1) blk_entry);
    block!      ((vm, qs_func_v1) blk_core);
    block!      ((vm, qs_func_v1) blk_exit);

    consta!     ((vm, qs_func_v1) part_funcref_local  = part_funcref);
    consta!     ((vm, qs_func_v1) qs_funcref_local  = qs_funcref);
    consta!     ((vm, qs_func_v1) int64_1_local  = int64_1);

    ssa!        ((vm, qs_func_v1) <array_ptr>   array);
    ssa!        ((vm, qs_func_v1) <int64>       low);
    ssa!        ((vm, qs_func_v1) <int64>       high);
    ssa!        ((vm, qs_func_v1) <int64>       pi); // partitioning index
    ssa!        ((vm, qs_func_v1) <int64>       before_pi); // pi-1
    ssa!        ((vm, qs_func_v1) <int64>       after_pi); // pi+1
    ssa!        ((vm, qs_func_v1) <int1>       cmp_res);

    /* BLOCK entry {
            if low < high {
                BRANCH partition
            }
            else {
                BRANCH exit
            }
    */
    inst!       ((vm, qs_func_v1) blk_init_lh_cmp:
        cmp_res = CMPOP (CmpOp::SLT) low high
    );
    inst!       ((vm, qs_func_v1) blk_init_branch2:
        BRANCH2 (cmp_res, array, low, high)
            IF (OP 0)
            THEN blk_core  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit (vec![])
    );

    /**
    BLOCK partition {
        pi = partition(array, low, high);
        calculate pi-1
        calculate pi+1
        call qs_func(low, pi-1)
        call qs_func(pi+1, high)
        branch exit
    }
    */
    inst!       ((vm, qs_func_v1) blk_core_partition:
        pi = EXPRCALL (CallConvention::Mu, is_abort: false)
            part_funcref_local (array, low, high)
    );
    inst!       ((vm, qs_func_v1) blk_core_pi_a:
        after_pi = BINOP (BinOp::Add) pi int64_1_local
    );
    inst!       ((vm, qs_func_v1) blk_core_pi_b:
        before_pi = BINOP (BinOp::Sub) pi int64_1_local
    );
    inst!       ((vm, qs_func_v1) blk_core_qs_low:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array, low, before_pi)
    );
    inst!       ((vm, qs_func_v1) blk_core_qs_high:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array, after_pi, high)
    );
    inst!       ((vm, qs_func_v1) blk_core_b_exit:
        BRANCH      blk_exit   ()
    );

    /**
    BLOCK exit {
        RET
    }
    */
    inst!       ((vm, qs_func_v1) blk_exit_exit:
        RET
    );

    define_block!   ((vm, qs_func_v1) blk_entry(array, low, high) {
        blk_init_lh_cmp,
        blk_init_branch2
    });

    define_block!   ((vm, qs_func_v1) blk_core(array, low, high) {
        blk_core_partition,
        blk_core_pi_a,
        blk_core_pi_b,
        blk_core_qs_low,
        blk_core_qs_high,
        blk_core_b_exit
    });

    define_block!   ((vm, qs_func_v1) blk_exit() {
        blk_exit_exit
    });

    define_func_ver!((vm) qs_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_exit
    });
    // ------------------------------------------------------------------
    // this function checks whether an array is sorted or not
    //
    //      fn cs_func(array, low, high) -> int64 (0 means sorted)
    // ------------------------------------------------------------------
    funcsig!    ((vm) cs_sig =    (array_ptr, int64, int64) -> (int64));
    funcdecl!   ((vm) <cs_sig>    cs_func);
    funcdef!    ((vm) <cs_sig>    cs_func VERSION cs_func_v1);

    block!      ((vm, cs_func_v1) blk_entry);
    block!      ((vm, cs_func_v1) blk_core);
    block!      ((vm, cs_func_v1) blk_exit);

    consta!     ((vm, cs_func_v1) int64_1_local  = int64_1);
    consta!     ((vm, cs_func_v1) pass_local  = int64_0);
    consta!     ((vm, cs_func_v1) fail_local  = int64_1);

    ssa!        ((vm, cs_func_v1) <array_ptr>   array);
    ssa!        ((vm, cs_func_v1) <int64>       low);
    ssa!        ((vm, cs_func_v1) <int64>       high);
    ssa!        ((vm, cs_func_v1) <int64_ptr>   elem_l_ptr);
    ssa!        ((vm, cs_func_v1) <int64_ptr>   elem_lpp_ptr);
    ssa!        ((vm, cs_func_v1) <int64>       elem_l);
    ssa!        ((vm, cs_func_v1) <int64>       elem_lpp);
    ssa!        ((vm, cs_func_v1) <int64>       lpp); // low++
    ssa!        ((vm, cs_func_v1) <int1>        cmp_res);
    ssa!        ((vm, cs_func_v1) <int64>       chk_res);

    // BLOCK entry
    inst!       ((vm, cs_func_v1) blk_entry_lpp:
        lpp = BINOP (BinOp::Add) low int64_1_local
    );
    inst!       ((vm, cs_func_v1) blk_entry_lh_cmp:
        cmp_res = CMPOP (CmpOp::SLE) lpp high
    );
    inst!       ((vm, cs_func_v1) blk_entry_branch2:
        BRANCH2 (cmp_res, array, low, high, pass_local)
            IF (OP 0)
            THEN blk_core  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit  (vec![4])
    );

    // BLOCK core
    inst!       ((vm, cs_func_v1) blk_core_elem_i_ptr:
        elem_l_ptr =  GETELEMIREF array low (is_ptr: true)
    );
    inst!       ((vm, cs_func_v1) blk_core_load_elem_i:
        elem_l = LOAD elem_l_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, cs_func_v1) blk_core_ipp:
        lpp = BINOP (BinOp::Add) low int64_1_local
    );
    inst!       ((vm, cs_func_v1) blk_core_elem_ipp_ptr:
        elem_lpp_ptr =  GETELEMIREF array lpp (is_ptr: true)
    );
    inst!       ((vm, cs_func_v1) blk_core_load_elem_ipp:
        elem_lpp = LOAD elem_lpp_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, cs_func_v1) blk_core_print_elem_ipp:
        PRINTHEX elem_lpp
    );
    inst!       ((vm, cs_func_v1) blk_core_lh_cmp:
        cmp_res = CMPOP (CmpOp::SGE) elem_lpp elem_l
    );
    inst!       ((vm, cs_func_v1) blk_core_branch2:
        BRANCH2 (cmp_res, array, lpp, high, fail_local)
            IF (OP 0)
            THEN blk_entry  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit  (vec![4])
    );

    // BLOCK exit
    inst!       ((vm, cs_func_v1) blk_exit_ret:
        RET (chk_res)
    );

    define_block!   ((vm, cs_func_v1) blk_entry(array, low, high) {
        blk_entry_lpp,
        blk_entry_lh_cmp,
        blk_entry_branch2
    });

    define_block!   ((vm, cs_func_v1) blk_core(array, low, high) {
        blk_core_elem_i_ptr,
        blk_core_load_elem_i,
        blk_core_ipp,
        blk_core_elem_ipp_ptr,
        blk_core_load_elem_ipp,
    blk_core_print_elem_ipp,
        blk_core_lh_cmp,
        blk_core_branch2
    });

    define_block!   ((vm, cs_func_v1) blk_exit(chk_res) {
        blk_exit_ret
    });

    define_func_ver!((vm) cs_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_exit
    });

    // ------------------------------------------------------------------
    // this function initializes the array and calls quicksort to sort it
    //
    //      fn init_func()
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) init_sig =    () -> (int64));
    funcdecl!   ((vm) <init_sig>    init_func);
    funcdef!    ((vm) <init_sig>    init_func VERSION init_func_v1);

    typedef!    ((vm) cs_sig_funcref = mu_funcref(cs_sig));
    constdef!   ((vm) <cs_sig_funcref> cs_funcref = Constant::FuncRef(cs_func));

    block!      ((vm, init_func_v1) blk_entry);
    block!      ((vm, init_func_v1) blk_init);
    block!      ((vm, init_func_v1) blk_call);

    consta!     ((vm, init_func_v1) array_size_local    = array_size);
    consta!     ((vm, init_func_v1) int64_0_local       = int64_0);
    consta!     ((vm, init_func_v1) int64_1_local       = int64_1);
    consta!     ((vm, init_func_v1) int64_BIG_local     = int64_BIG);
    consta!     ((vm, init_func_v1) qs_funcref_local    = qs_funcref);
    consta!     ((vm, init_func_v1) cs_funcref_local    = cs_funcref);

    ssa!        ((vm, init_func_v1) <array_ptr>     array);
    ssa!        ((vm, init_func_v1) <array_ptr>     array_allocated);
    ssa!        ((vm, init_func_v1) <array_ptr>     array_inited);
    ssa!        ((vm, init_func_v1) <int64>         idx);
    ssa!        ((vm, init_func_v1) <int64>         array_len);

    ssa!        ((vm, init_func_v1) <int64>         elem_val);
    ssa!        ((vm, init_func_v1) <int64_ptr>     elem_ptr);
    ssa!        ((vm, init_func_v1) <int64>         next_idx);
    ssa!        ((vm, init_func_v1) <int1>          cmp_res);
    ssa!        ((vm, init_func_v1) <int64>         chk_res);
    ssa!        ((vm, init_func_v1) <int64>         low_index);
    ssa!        ((vm, init_func_v1) <int64>         high_index);

    /*
    BLOCK entry {
        ealloc array
        print array_ptr
        BRANCH  blk_init(array, 0, array_size)
    */
    inst_rt!       ((vm, init_func_v1) blk_entry_ealloc_array:
        array  =   EALLOC  array_int64
    );
    //      print _array content as hex
    inst!       ((vm, init_func_v1) blk_entry_print_array:
        PRINTHEX    array
    );
    inst!       ((vm, init_func_v1) blk_entry_branch:
        BRANCH      blk_init   (array, int64_0_local, array_size_local)
    );

    /*
    BLOCK init {
        array[i] = random
        i++
        if i < array_size
            BRANCH  blk_init(array, i, array_size)
        else
            BRANCH  blk_call(array)
    }
    */
    inst!       ((vm, init_func_v1) blk_init_get_elem_val:
        elem_val =  RANDI   int64_0_local,  int64_BIG_local
    );

    inst!       ((vm, init_func_v1) blk_init_get_elem_ptr:
        elem_ptr =  GETELEMIREF array_allocated idx (is_ptr: true)
    );

    inst!       ((vm, init_func_v1) blk_init_init_elem:
        STORE elem_ptr elem_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    inst!       ((vm, init_func_v1) blk_init_next_elem:
        next_idx = BINOP (BinOp::Add) idx int64_1_local
    );

    inst!       ((vm, init_func_v1) blk_init_idx_cmp:
        cmp_res = CMPOP (CmpOp::ULT) next_idx array_size_local
    );

    inst!       ((vm, init_func_v1) blk_init_branch2:
        BRANCH2 (cmp_res, array, next_idx, array_size_local)
            IF (OP 0)
            THEN blk_init  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_call (vec![1])
    );

    /*
    BLOCK call {
        call qs(initialized_array)
        EXIT
    }
    */
    inst!       ((vm, init_func_v1) blk_call_low_idx:
        low_index = BINOP (BinOp::Add) int64_0_local int64_0_local
    );
    inst!       ((vm, init_func_v1) blk_call_high_idx:
        high_index = BINOP (BinOp::Sub) array_size_local int64_1_local
    );
    inst!       ((vm, init_func_v1) blk_call_call_qs:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array_inited, low_index, high_index)
    );
    inst!       ((vm, init_func_v1) blk_call_call_cs:
        chk_res = EXPRCALL (CallConvention::Mu, is_abort: false)
            cs_funcref_local (array_inited, low_index, high_index)
    );

    inst_rt!       ((vm, init_func_v1) blk_call_dealloc_array:
        EDELETE array_inited
    );
    inst!       ((vm, init_func_v1) blk_call_exit:
        RET (chk_res)
    );

    define_block!   ((vm, init_func_v1) blk_entry() {
        blk_entry_ealloc_array,
        blk_entry_print_array,
        blk_entry_branch
    });

    define_block!   ((vm, init_func_v1) blk_init(array_allocated, idx, array_len) {
        blk_init_get_elem_val,
        blk_init_get_elem_ptr,
        blk_init_init_elem,
        blk_init_next_elem,
        blk_init_idx_cmp,
        blk_init_branch2
    });

    define_block!   ((vm, init_func_v1) blk_call(array_inited) {
    blk_call_low_idx,
    blk_call_high_idx,
        blk_call_call_qs,
        blk_call_call_cs,
        blk_call_dealloc_array,
        blk_call_exit
    });

    define_func_ver!((vm) init_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_init,
        blk_call
    });

    emit_timed_test!((vm)
        TESTER  qs_ealloc_tester_1,
        TESTEE  init_func,
        INPUTS  (),
        EXPECTED    EQ  int64(0),
    );

    vm
}
fn rtmu_quicksort_ralloc(__array_size: usize) -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int1             = mu_int    (1));
    typedef!    ((vm) int64             = mu_int    (64));
    typedef!    ((vm) int64_ptr         = mu_uptr   (int64));
    typedef!    ((vm) array_int64       = mu_array  (int64, __array_size));
    typedef!    ((vm) array_ptr         = mu_uptr   (array_int64));

    constdef!   ((vm) <int64> array_size    = Constant::Int((__array_size) as u64));
    constdef!   ((vm) <int64> int64_0       = Constant::Int(0));
    constdef!   ((vm) <int64> int64_1       = Constant::Int(1));
    constdef!   ((vm) <int64> int64_8       = Constant::Int(8));
    constdef!   ((vm) <int64> int64_BIG       = Constant::Int(1000000));

    // ------------------------------------------------------------------
    // the quicksort partition function
    //
    //     fn part_func(array, low, high) -> partition_index
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) part_sig =    (array_ptr, int64, int64) -> (int64));
    funcdecl!   ((vm) <part_sig>    part_func);
    funcdef!    ((vm) <part_sig>    part_func VERSION part_func_v1);

    ssa!        ((vm, part_func_v1) <array_ptr>   array);
    ssa!        ((vm, part_func_v1) <int64>       low);
    ssa!        ((vm, part_func_v1) <int64>       high);

    ssa!        ((vm, part_func_v1) <int64_ptr>   pi_ptr);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_val);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_i_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_i_val);
    ssa!        ((vm, part_func_v1) <int64_ptr>   elem_j_ptr);
    ssa!        ((vm, part_func_v1) <int64>       elem_j_val);
    ssa!        ((vm, part_func_v1) <int64>       swap_temp);
    ssa!        ((vm, part_func_v1) <int64>       pi);
    ssa!        ((vm, part_func_v1) <int64>       ic); // pivot counter
    ssa!        ((vm, part_func_v1) <int64>       next_ic); // ic++
    ssa!        ((vm, part_func_v1) <int64>       jc); // tracer counter
    ssa!        ((vm, part_func_v1) <int64>       next_jc); // jc++
    ssa!        ((vm, part_func_v1) <int1>        cmp_res);

    block!      ((vm, part_func_v1) blk_entry);
    block!      ((vm, part_func_v1) blk_core);
    block!      ((vm, part_func_v1) blk_core_proc);
    block!      ((vm, part_func_v1) blk_core_proc_swap);
    block!      ((vm, part_func_v1) blk_core_proc_loop);
    block!      ((vm, part_func_v1) blk_exit);

    consta!     ((vm, part_func_v1) int64_1_local  = int64_1);

    /*
    BLOCK entry {
        pi = array[high]
        ic = low - 1
        BRANCH core_loop(pi, ic)
    }
    */
    inst!       ((vm, part_func_v1) blk_entry_get_pi_ptr:
        pi_ptr =  GETELEMIREF array high (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_entry_load_pi:
        pi = LOAD pi_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_entry_calc_ic:
        ic = BINOP (BinOp::Sub) low int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_entry_calc_jc:
        jc = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_entry_b_core:
        BRANCH      blk_core   (array, low, high, pi, ic, jc)
    );

    /*
    BLOCK core {
        if jcounter < high
            BRANCH core_proc
        else
            BRANCH exit
    }
    */
    inst!       ((vm, part_func_v1) blk_core_cmp:
        cmp_res = CMPOP (CmpOp::ULT) jc high
    );
    inst!       ((vm, part_func_v1) blk_core_branch2:
        BRANCH2 (cmp_res, array, low, high, pi, ic, jc)
            IF (OP 0)
            THEN blk_core_proc  (vec![1, 2, 3, 4, 5, 6]) WITH 0.9f32,
            ELSE blk_exit (vec![1, 3, 5])
    );

    /*
    BLOCK core_proc {
        if array[jc] <= pi
            BRANCH  core_proc_swap
        else
            BRANCH  core_proc_loop
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_elem_ptr:
        elem_ptr =  GETELEMIREF array jc (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_load_elem:
        elem_val = LOAD elem_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_cmp:
        cmp_res = CMPOP (CmpOp::ULE) elem_val pi
    );
    inst!       ((vm, part_func_v1) blk_core_proc_branch2:
        BRANCH2 (cmp_res, array, low, high, pi, ic, jc)
            IF (OP 0)
            THEN blk_core_proc_swap  (vec![1, 2, 3, 4, 5, 6]) WITH 0.9f32,
            ELSE blk_core_proc_loop (vec![1, 2, 3, 4, 5, 6])
    );

    /*
    BLOCK   core_proc_swap {
        ic++
        if array[jc] <= pivot
            swap_elements(i,j)
        BRANCH core_proc_loop
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_swap_ipp:
        next_ic = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_elem_i_ptr:
        elem_i_ptr =  GETELEMIREF array next_ic (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_load_elem_i:
        elem_i_val = LOAD elem_i_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_elem_j_ptr:
        elem_j_ptr =  GETELEMIREF array jc (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_load_elem_j:
        elem_j_val = LOAD elem_j_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_swap_elem_i:
        STORE elem_i_ptr elem_j_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_swap_elem_j:
        STORE elem_j_ptr elem_i_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_core_proc_swap_b_loop:
        BRANCH      blk_core_proc_loop   (array, low, high, pi, next_ic, jc)
    );

    /*
    BLOCK core_proc_loop {
        jc++
        BRANCH core
    }
    */
    inst!       ((vm, part_func_v1) blk_core_proc_loop_jpp:
        next_jc = BINOP (BinOp::Add) jc int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_core_proc_loop_b_core:
        BRANCH      blk_core   (array, low, high, pi, ic, next_jc)
    );

    /*
    BLOCK exit {
        swap_elements(i+1, high)
        ret (i+1)
    }
    */
    inst!       ((vm, part_func_v1) blk_exit_ipp:
        next_ic = BINOP (BinOp::Add) ic int64_1_local
    );
    inst!       ((vm, part_func_v1) blk_exit_elem_i_ptr:
        elem_i_ptr =  GETELEMIREF array next_ic (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_exit_load_elem_i:
        elem_i_val = LOAD elem_i_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_elem_high_ptr:
        elem_j_ptr =  GETELEMIREF array high (is_ptr: true)
    );
    inst!       ((vm, part_func_v1) blk_exit_load_elem_high:
        elem_j_val = LOAD elem_j_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_swap_elem_i:
        STORE elem_i_ptr elem_j_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_swap_elem_high:
        STORE elem_j_ptr elem_i_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, part_func_v1) blk_exit_ret:
        RET (next_ic)
    );

    define_block!   ((vm, part_func_v1) blk_entry(array, low, high) {
        blk_entry_get_pi_ptr,
        blk_entry_load_pi,
        blk_entry_calc_ic,
        blk_entry_calc_jc,
        blk_entry_b_core
    });
    define_block!   ((vm, part_func_v1) blk_core(array, low, high, pi, ic, jc) {
        blk_core_cmp,
        blk_core_branch2
    });
    define_block!   ((vm, part_func_v1) blk_core_proc(array, low, high, pi, ic, jc) {
        blk_core_proc_elem_ptr,
        blk_core_proc_load_elem,
        blk_core_proc_cmp,
        blk_core_proc_branch2
    });
    define_block!   ((vm, part_func_v1) blk_core_proc_swap(array, low, high, pi, ic, jc) {
        blk_core_proc_swap_ipp,
        blk_core_proc_swap_elem_i_ptr,
        blk_core_proc_swap_load_elem_i,
        blk_core_proc_swap_elem_j_ptr,
        blk_core_proc_swap_load_elem_j,
        blk_core_proc_swap_swap_elem_i,
        blk_core_proc_swap_swap_elem_j,
        blk_core_proc_swap_b_loop
    });
    define_block!   ((vm, part_func_v1) blk_core_proc_loop(array, low, high, pi, ic, jc) {
        blk_core_proc_loop_jpp,
        blk_core_proc_loop_b_core
    });
    define_block!   ((vm, part_func_v1) blk_exit(array, high, ic) {
        blk_exit_ipp,
        blk_exit_elem_i_ptr,
        blk_exit_load_elem_i,
        blk_exit_elem_high_ptr,
        blk_exit_load_elem_high,
        blk_exit_swap_elem_i,
        blk_exit_swap_elem_high,
        blk_exit_ret
    });
    define_func_ver!((vm) part_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_core_proc,
        blk_core_proc_swap,
        blk_core_proc_loop,
        blk_exit
    });
    // ------------------------------------------------------------------

    typedef!    ((vm) part_sig_funcref = mu_funcref(part_sig));
    constdef!   ((vm) <part_sig_funcref> part_funcref = Constant::FuncRef(part_func));

    // ------------------------------------------------------------------
    // the basic quicksort function
    //
    //      fn qs_func(array, low, high)
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) qs_sig =    (array_ptr, int64, int64) -> ());
    funcdecl!   ((vm) <qs_sig>    qs_func);
    funcdef!    ((vm) <qs_sig>    qs_func VERSION qs_func_v1);

    typedef!    ((vm) qs_sig_funcref = mu_funcref(qs_sig));
    constdef!   ((vm) <qs_sig_funcref> qs_funcref = Constant::FuncRef(qs_func));

    block!      ((vm, qs_func_v1) blk_entry);
    block!      ((vm, qs_func_v1) blk_core);
    block!      ((vm, qs_func_v1) blk_exit);

    consta!     ((vm, qs_func_v1) part_funcref_local  = part_funcref);
    consta!     ((vm, qs_func_v1) qs_funcref_local  = qs_funcref);
    consta!     ((vm, qs_func_v1) int64_1_local  = int64_1);

    ssa!        ((vm, qs_func_v1) <array_ptr>   array);
    ssa!        ((vm, qs_func_v1) <int64>       low);
    ssa!        ((vm, qs_func_v1) <int64>       high);
    ssa!        ((vm, qs_func_v1) <int64>       pi); // partitioning index
    ssa!        ((vm, qs_func_v1) <int64>       before_pi); // pi-1
    ssa!        ((vm, qs_func_v1) <int64>       after_pi); // pi+1
    ssa!        ((vm, qs_func_v1) <int1>       cmp_res);

    /* BLOCK entry {
            if low < high {
                BRANCH partition
            }
            else {
                BRANCH exit
            }
    */
    inst!       ((vm, qs_func_v1) blk_init_lh_cmp:
        cmp_res = CMPOP (CmpOp::SLT) low high
    );
    inst!       ((vm, qs_func_v1) blk_init_branch2:
        BRANCH2 (cmp_res, array, low, high)
            IF (OP 0)
            THEN blk_core  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit (vec![])
    );

    /**
    BLOCK partition {
        pi = partition(array, low, high);
        calculate pi-1
        calculate pi+1
        call qs_func(low, pi-1)
        call qs_func(pi+1, high)
        branch exit
    }
    */
    inst!       ((vm, qs_func_v1) blk_core_partition:
        pi = EXPRCALL (CallConvention::Mu, is_abort: false)
            part_funcref_local (array, low, high)
    );
    inst!       ((vm, qs_func_v1) blk_core_pi_a:
        after_pi = BINOP (BinOp::Add) pi int64_1_local
    );
    inst!       ((vm, qs_func_v1) blk_core_pi_b:
        before_pi = BINOP (BinOp::Sub) pi int64_1_local
    );
    inst!       ((vm, qs_func_v1) blk_core_qs_low:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array, low, before_pi)
    );
    inst!       ((vm, qs_func_v1) blk_core_qs_high:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array, after_pi, high)
    );
    inst!       ((vm, qs_func_v1) blk_core_b_exit:
        BRANCH      blk_exit   ()
    );

    /**
    BLOCK exit {
        RET
    }
    */
    inst!       ((vm, qs_func_v1) blk_exit_exit:
        RET
    );

    define_block!   ((vm, qs_func_v1) blk_entry(array, low, high) {
        blk_init_lh_cmp,
        blk_init_branch2
    });

    define_block!   ((vm, qs_func_v1) blk_core(array, low, high) {
        blk_core_partition,
        blk_core_pi_a,
        blk_core_pi_b,
        blk_core_qs_low,
        blk_core_qs_high,
        blk_core_b_exit
    });

    define_block!   ((vm, qs_func_v1) blk_exit() {
        blk_exit_exit
    });

    define_func_ver!((vm) qs_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_exit
    });
    // ------------------------------------------------------------------
    // this function checks whether an array is sorted or not
    //
    //      fn cs_func(array, low, high) -> int64 (0 means sorted)
    // ------------------------------------------------------------------
    funcsig!    ((vm) cs_sig =    (array_ptr, int64, int64) -> (int64));
    funcdecl!   ((vm) <cs_sig>    cs_func);
    funcdef!    ((vm) <cs_sig>    cs_func VERSION cs_func_v1);

    block!      ((vm, cs_func_v1) blk_entry);
    block!      ((vm, cs_func_v1) blk_core);
    block!      ((vm, cs_func_v1) blk_exit);

    consta!     ((vm, cs_func_v1) int64_1_local  = int64_1);
    consta!     ((vm, cs_func_v1) pass_local  = int64_0);
    consta!     ((vm, cs_func_v1) fail_local  = int64_1);

    ssa!        ((vm, cs_func_v1) <array_ptr>   array);
    ssa!        ((vm, cs_func_v1) <int64>       low);
    ssa!        ((vm, cs_func_v1) <int64>       high);
    ssa!        ((vm, cs_func_v1) <int64_ptr>   elem_l_ptr);
    ssa!        ((vm, cs_func_v1) <int64_ptr>   elem_lpp_ptr);
    ssa!        ((vm, cs_func_v1) <int64>       elem_l);
    ssa!        ((vm, cs_func_v1) <int64>       elem_lpp);
    ssa!        ((vm, cs_func_v1) <int64>       lpp); // low++
    ssa!        ((vm, cs_func_v1) <int1>        cmp_res);
    ssa!        ((vm, cs_func_v1) <int64>       chk_res);

    // BLOCK entry
    inst!       ((vm, cs_func_v1) blk_entry_lpp:
        lpp = BINOP (BinOp::Add) low int64_1_local
    );
    inst!       ((vm, cs_func_v1) blk_entry_lh_cmp:
        cmp_res = CMPOP (CmpOp::SLE) lpp high
    );
    inst!       ((vm, cs_func_v1) blk_entry_branch2:
        BRANCH2 (cmp_res, array, low, high, pass_local)
            IF (OP 0)
            THEN blk_core  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit  (vec![4])
    );

    // BLOCK core
    inst!       ((vm, cs_func_v1) blk_core_elem_i_ptr:
        elem_l_ptr =  GETELEMIREF array low (is_ptr: true)
    );
    inst!       ((vm, cs_func_v1) blk_core_load_elem_i:
        elem_l = LOAD elem_l_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, cs_func_v1) blk_core_ipp:
        lpp = BINOP (BinOp::Add) low int64_1_local
    );
    inst!       ((vm, cs_func_v1) blk_core_elem_ipp_ptr:
        elem_lpp_ptr =  GETELEMIREF array lpp (is_ptr: true)
    );
    inst!       ((vm, cs_func_v1) blk_core_load_elem_ipp:
        elem_lpp = LOAD elem_lpp_ptr (is_ptr: true, order: MemoryOrder::Relaxed)
    );
    inst!       ((vm, cs_func_v1) blk_core_print_elem_ipp:
        PRINTHEX elem_lpp
    );
    inst!       ((vm, cs_func_v1) blk_core_lh_cmp:
        cmp_res = CMPOP (CmpOp::SGE) elem_lpp elem_l
    );
    inst!       ((vm, cs_func_v1) blk_core_branch2:
        BRANCH2 (cmp_res, array, lpp, high, fail_local)
            IF (OP 0)
            THEN blk_entry  (vec![1, 2, 3]) WITH 0.9f32,
            ELSE blk_exit  (vec![4])
    );

    // BLOCK exit
    inst!       ((vm, cs_func_v1) blk_exit_ret:
        RET (chk_res)
    );

    define_block!   ((vm, cs_func_v1) blk_entry(array, low, high) {
        blk_entry_lpp,
        blk_entry_lh_cmp,
        blk_entry_branch2
    });

    define_block!   ((vm, cs_func_v1) blk_core(array, low, high) {
        blk_core_elem_i_ptr,
        blk_core_load_elem_i,
        blk_core_ipp,
        blk_core_elem_ipp_ptr,
        blk_core_load_elem_ipp,
    blk_core_print_elem_ipp,
        blk_core_lh_cmp,
        blk_core_branch2
    });

    define_block!   ((vm, cs_func_v1) blk_exit(chk_res) {
        blk_exit_ret
    });

    define_func_ver!((vm) cs_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_core,
        blk_exit
    });

    // ------------------------------------------------------------------
    // this function initializes the array and calls quicksort to sort it
    //
    //      fn init_func()
    //
    // ------------------------------------------------------------------
    funcsig!    ((vm) init_sig =    () -> (int64));
    funcdecl!   ((vm) <init_sig>    init_func);
    funcdef!    ((vm) <init_sig>    init_func VERSION init_func_v1);

    typedef!    ((vm) regionref_t = mu_regionref);
    typedef!    ((vm) cs_sig_funcref = mu_funcref(cs_sig));
    constdef!   ((vm) <cs_sig_funcref> cs_funcref = Constant::FuncRef(cs_func));

    block!      ((vm, init_func_v1) blk_entry);
    block!      ((vm, init_func_v1) blk_init);
    block!      ((vm, init_func_v1) blk_call);

    consta!     ((vm, init_func_v1) array_size_local    = array_size);
    consta!     ((vm, init_func_v1) int64_0_local       = int64_0);
    consta!     ((vm, init_func_v1) int64_1_local       = int64_1);
    consta!     ((vm, init_func_v1) int64_8_local       = int64_8);
    consta!     ((vm, init_func_v1) int64_BIG_local     = int64_BIG);
    consta!     ((vm, init_func_v1) qs_funcref_local    = qs_funcref);
    consta!     ((vm, init_func_v1) cs_funcref_local    = cs_funcref);

    ssa!        ((vm, init_func_v1) <regionref_t>   reg_ref);
    ssa!        ((vm, init_func_v1) <int64>         reg_size);
    ssa!        ((vm, init_func_v1) <array_ptr>     array);
    ssa!        ((vm, init_func_v1) <array_ptr>     array_allocated);
    ssa!        ((vm, init_func_v1) <array_ptr>     array_inited);
    ssa!        ((vm, init_func_v1) <int64>         idx);
    ssa!        ((vm, init_func_v1) <int64>         array_len);

    ssa!        ((vm, init_func_v1) <int64>         elem_val);
    ssa!        ((vm, init_func_v1) <int64_ptr>     elem_ptr);
    ssa!        ((vm, init_func_v1) <int64>         next_idx);
    ssa!        ((vm, init_func_v1) <int1>          cmp_res);
    ssa!        ((vm, init_func_v1) <int64>         chk_res);
    ssa!        ((vm, init_func_v1) <int64>         low_index);
    ssa!        ((vm, init_func_v1) <int64>         high_index);

    /*
    BLOCK entry {
        ealloc array
        print array_ptr
        BRANCH  blk_init(array, 0, array_size)
    */
    inst!       ((vm, init_func_v1) blk_entry_reg_size:
        reg_size = BINOP (BinOp::Mul) array_size_local int64_8_local
    );
    inst_rt!       ((vm, init_func_v1) blk_entry_new_region:
        reg_ref =   NEWREGION  reg_size
    );
    inst!       ((vm, init_func_v1) blk_entry_print_region:
        PRINTHEX    reg_ref
    );
    inst_rt!       ((vm, init_func_v1) blk_entry_ralloc_array:
        array  =   RALLOC  reg_ref, array_int64
    );
    //      print _array content as hex
    inst!       ((vm, init_func_v1) blk_entry_print_array:
        PRINTHEX    array
    );
    inst!       ((vm, init_func_v1) blk_entry_branch:
        BRANCH      blk_init   (reg_ref, array, int64_0_local, array_size_local)
    );

    /*
    BLOCK init {
        array[i] = random
        i++
        if i < array_size
            BRANCH  blk_init(array, i, array_size)
        else
            BRANCH  blk_call(array)
    }
    */
    inst!       ((vm, init_func_v1) blk_init_get_elem_val:
        elem_val =  RANDI   int64_0_local,  int64_BIG_local
    );

    inst!       ((vm, init_func_v1) blk_init_get_elem_ptr:
        elem_ptr =  GETELEMIREF array_allocated idx (is_ptr: true)
    );

    inst!       ((vm, init_func_v1) blk_init_init_elem:
        STORE elem_ptr elem_val (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    inst!       ((vm, init_func_v1) blk_init_next_elem:
        next_idx = BINOP (BinOp::Add) idx int64_1_local
    );

    inst!       ((vm, init_func_v1) blk_init_idx_cmp:
        cmp_res = CMPOP (CmpOp::ULT) next_idx array_size_local
    );

    inst!       ((vm, init_func_v1) blk_init_branch2:
        BRANCH2 (cmp_res, reg_ref, array, next_idx, array_size_local)
            IF (OP 0)
            THEN blk_init  (vec![1, 2, 3, 4]) WITH 0.9f32,
            ELSE blk_call (vec![1, 2])
    );

    /*
    BLOCK call {
        call qs(initialized_array)
        call cs(sorted_array)
        EXIT
    }
    */
    inst!       ((vm, init_func_v1) blk_call_low_idx:
        low_index = BINOP (BinOp::Add) int64_0_local int64_0_local
    );
    inst!       ((vm, init_func_v1) blk_call_high_idx:
        high_index = BINOP (BinOp::Sub) array_size_local int64_1_local
    );
    inst!       ((vm, init_func_v1) blk_call_call_qs:
        EXPRCALL (CallConvention::Mu, is_abort: false)
            qs_funcref_local (array_inited, low_index, high_index)
    );
    inst!       ((vm, init_func_v1) blk_call_call_cs:
        chk_res = EXPRCALL (CallConvention::Mu, is_abort: false)
            cs_funcref_local (array_inited, low_index, high_index)
    );
    inst_rt!       ((vm, init_func_v1) blk_call_delete_region:
        DELETEREGION reg_ref
    );
    inst!       ((vm, init_func_v1) blk_call_exit:
        RET (chk_res)
    );

    define_block!   ((vm, init_func_v1) blk_entry() {
        blk_entry_reg_size,
        blk_entry_new_region,
        blk_entry_print_region,
        blk_entry_ralloc_array,
        blk_entry_print_array,
        blk_entry_branch
    });

    define_block!   ((vm, init_func_v1) blk_init(reg_ref, array_allocated, idx, array_len) {
        blk_init_get_elem_val,
        blk_init_get_elem_ptr,
        blk_init_init_elem,
        blk_init_next_elem,
        blk_init_idx_cmp,
        blk_init_branch2
    });

    define_block!   ((vm, init_func_v1) blk_call(reg_ref, array_inited) {
    blk_call_low_idx,
    blk_call_high_idx,
        blk_call_call_qs,
        blk_call_call_cs,
        blk_call_delete_region,
        blk_call_exit
    });

    define_func_ver!((vm) init_func_v1 (entry: blk_entry) {
        blk_entry,
        blk_init,
        blk_call
    });

    emit_timed_test!((vm)
        TESTER  qs_ralloc_tester_1,
        TESTEE  init_func,
        INPUTS  (),
        EXPECTED    EQ  int64(0),
    );

    vm
}
