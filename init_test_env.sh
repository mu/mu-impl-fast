#!/bin/sh
export MU_ZEBU=$PWD
export ZEBU_BUILD=debug
export CARGO_HOME=~/.cargo
export CC=clang
export CXX=clang++
export RUST_TEST_THREADS=1
export RUST_BACKTRACE=1
export MU_LOG_LEVEL=trace
export LD_LIBRARY_PATH=$MU_ZEBU/target/$ZEBU_BUILD/deps/:$LD_LIBRARY_PATH
