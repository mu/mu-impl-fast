// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub(super) use super::thread_posix::*;

pub unsafe fn sys_thread_launch(
    muthread: Box<MuThread>,
    new_sp: Address,
    exception: Option<Address>
) -> SysResult {
    let _sys_thread_id =
        &muthread.sys_thread_id as *const SysThreadID as *mut SysThreadID;
    let exception = {
        if exception.is_none() {
            Address::from_ptr(std::ptr::null() as *const Address)
        } else {
            exception.unwrap()
        }
    };

    let muthread_ptr = Box::into_raw(muthread);
    let muthread_add = Address::from_mut_ptr(muthread_ptr);
    let mut muthread_box = Box::from_raw(muthread_ptr);

    let context = Box::new(MuThreadContext {
        muthread: muthread_add,
        new_sp,
        exception
    });
    let context = Box::into_raw(context);
    muthread_box.sys_thread_context =
        context as *const MuThreadContext as *const SysVoid as *mut SysVoid;

    let muthread_ptr = Box::into_raw(muthread_box);

    let res = libc::pthread_create(
        _sys_thread_id,
        SYSNULL as *const SysThreadAttr,
        _pthread_entry_point,
        muthread_ptr as *mut SysVoid
    );

    res
}
