// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// MU (non-real-time) specific threading stuff goes to this module
/// Currently it's empty
use super::*;

//pub(super) use super::thread_posix::*;
pub(super) use super::thread_mu_posix::*;
// use core::borrow::BorrowMut;

impl MuThread {
    /// creates and launches a mu thread, returns a JoinHandle and address to
    /// its MuThread structure
    pub(super) fn mu_thread_launch(
        id: MuID,
        stack: Box<MuStack>,
        user_tls: Address,
        exception: Option<Address>,
        vm: Arc<VM>
    ) -> *mut MuThread {
        let new_sp = stack.sp;

        // FIXME - not all threads need a mutator
        let mut thread: Box<MuThread> =
            Box::new(MuThread::new(id, mm::new_mutator(), stack, user_tls, vm));
        {
            // set mutator for each allocator
            let mutator_ptr = &mut thread.allocator as *mut mm::Mutator;
            thread.allocator.update_mutator_ptr(mutator_ptr);
        }

        // we need to return the pointer, but we cannot send it to other thread
        let muthread_ptr = Box::into_raw(thread);
        let muthread = unsafe { Box::from_raw(muthread_ptr) };

        let res = unsafe { sys_thread_launch(muthread, new_sp, exception) };
        assert_eq!(res, MU_SUCCESS);

        muthread_ptr
    }
}

// Creates a new thread
#[no_mangle]
pub unsafe extern "C" fn muentry_new_thread_exceptional(
    stack: *mut MuStack,
    thread_local: Address,
    exception: Address
) -> *mut MuThread {
    let vm = MuThread::current_mut().vm.clone();
    let muthread = MuThread::mu_thread_launch(
        vm.next_id(),
        Box::from_raw(stack),
        thread_local,
        Some(exception),
        vm.clone()
    );
    vm.push_join_handle(muthread);
    muthread
}

// Creates a new thread
#[no_mangle]
pub unsafe extern "C" fn muentry_new_thread_normal(
    stack: *mut MuStack,
    thread_local: Address
) -> *mut MuThread {
    let vm = MuThread::current_mut().vm.clone();
    let muthread = MuThread::mu_thread_launch(
        vm.next_id(),
        Box::from_raw(stack),
        thread_local,
        None,
        vm.clone()
    );
    vm.push_join_handle(muthread);
    muthread
}
