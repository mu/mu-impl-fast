// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

use std::sync::atomic::{AtomicU32, Ordering};

use super::*;
// use super::futex_rtmu::*;

pub(super) type OsTidT = libc::pid_t;

#[repr(C)]
#[repr(align(8))]
pub(super) struct sys_rtmu_futex {
    pub mem: AtomicU32
}

impl sys_rtmu_futex {
    pub fn new() -> Address {
        let fbox = Box::new(sys_rtmu_futex {
            mem: AtomicU32::new(0)
        });

        Address::from_mut_ptr(Box::into_raw(fbox))
    }

    pub fn delete(f: Address) {
        unsafe {
            Box::from_raw(Address::to_ptr_mut(&f) as *mut sys_rtmu_futex)
        };
    }

    pub fn lock(&mut self, timeout_ns: u64) {
        let my_tid = os_gettid();
        match self.mem.compare_exchange(
            0 as u32,
            my_tid as u32,
            Ordering::SeqCst,
            Ordering::SeqCst
        ) {
            Ok(_) => {
                trace!("FUTEX.lock fastpath success");
            }
            Err(id) => {
                trace!(
                    "FUTEX.lock fastpath failed: #{:#?} already locked it",
                    id
                );
                sys_futex_lock_slowpath(
                    &mut self.mem as *const AtomicU32 as *const u32 as *mut u32,
                    timeout_ns
                );
            }
        }
    }

    pub fn unlock(&mut self) {
        let my_tid = os_gettid();
        match self.mem.compare_exchange(
            my_tid as u32,
            0 as u32,
            Ordering::SeqCst,
            Ordering::SeqCst
        ) {
            Ok(_) => {
                trace!("FUTEX.unlock fastpath success");
            }
            Err(id) => {
                trace!(
                    "FUTEX.unlock fastpath failed: futex content is #{:#?}",
                    id
                );
                sys_futex_unlock_slowpath(
                    &mut self.mem as *const AtomicU32 as *const u32 as *mut u32
                );
            }
        }
    }
}

pub fn os_gettid() -> OsTidT {
    unsafe { libc::syscall(libc::SYS_gettid) as OsTidT }
}

fn sys_futex_lock_slowpath(futex_ptr: *mut u32, timeout_ns: u64) {
    let null_ts: *mut libc::timespec = std::ptr::null_mut();
    let null_cl: *mut libc::c_long = std::ptr::null_mut();

    let res = match timeout_ns {
        0 => unsafe {
            libc::syscall(
                libc::SYS_futex,
                futex_ptr,
                libc::FUTEX_LOCK_PI,
                0,
                null_ts,
                null_cl,
                0
            )
        },
        _ => unsafe {
            libc::syscall(
                libc::SYS_futex,
                futex_ptr,
                libc::FUTEX_LOCK_PI,
                0,
                &ns_to_time(timeout_ns) as *const libc::timespec,
                null_cl,
                0
            )
        }
    };
    assert_eq!(
        res, 0,
        "FUTEX.lock slowpath failed with error code #{}",
        res
    );
    trace!("FUTEX.lock slowpath aquired #{:#?}", futex_ptr);
}

fn sys_futex_unlock_slowpath(futex_ptr: *mut u32) {
    let null_ts: *mut libc::timespec = std::ptr::null_mut();
    let null_cl: *mut libc::c_long = std::ptr::null_mut();

    let res = unsafe {
        libc::syscall(
            libc::SYS_futex,
            futex_ptr,
            libc::FUTEX_UNLOCK_PI,
            0,
            null_ts,
            null_cl,
            0
        )
    };
    assert_eq!(
        res, 0,
        "FUTEX.unlock slowpath failed with error code #{}",
        res
    );
    trace!("FUTEX.unlock slowpath released #{:#?}", futex_ptr);
}

fn ns_to_time(ns: u64) -> libc::timespec {
    libc::timespec {
        tv_sec: (ns / 1_000_000_000) as i64,
        tv_nsec: (ns % 1_000_000_000) as i64
    }
}
