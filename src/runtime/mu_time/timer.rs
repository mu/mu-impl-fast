// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//use super::timer_timer_crate::*;
use super::timer_posix::*;
use super::*;

pub type RTMuTimer = SysTimer;

#[no_mangle]
pub extern "C" fn muentry_new_timer() -> Address {
    Address::from_mut_ptr(SysTimer::new())
}

#[no_mangle]
pub extern "C" fn muentry_set_timer(
    tmr: Address,
    tm: clock::TimeValT,
    prd: clock::TimeValT,
    func: Address,
    args: Address
) {
    trace!(
        "muentry_set_timer ({}, {}, {}, {}, {})",
        tmr,
        tm,
        prd,
        func,
        args
    );
    let mut _timerref: Box<RTMuTimer> =
        unsafe { Box::from_raw(tmr.to_ptr_mut()) };
    _timerref.set(tm, prd, func, args);
    let _tmr = Box::into_raw(_timerref);
}

#[no_mangle]
pub extern "C" fn muentry_cancel_timer(tmr: Address) {
    let mut _timerref: Box<RTMuTimer> =
        unsafe { Box::from_raw(tmr.to_ptr_mut()) };
    _timerref.cancel();
    let _tmr = Box::into_raw(_timerref);
}

#[no_mangle]
pub extern "C" fn muentry_delete_timer(tmr: Address) {
    let mut _timerref: Box<RTMuTimer> =
        unsafe { Box::from_raw(tmr.to_ptr_mut()) };
    _timerref.delete();
}

#[no_mangle]
pub extern "C" fn muentry_sleep_ns(dur: usize) {
    sys_sleep_ns(dur as u64);
}

pub(super) fn sys_sleep_ns(dur: u64) {
    use std::{thread, time};

    let now: Option<std::time::Instant> = if cfg!(debug_assertions) {
        let now = time::Instant::now();
        trace!("sleeping at #{:#?}", now);
        Some(now)
    } else {
        None
    };

    let duration = time::Duration::from_nanos(dur);
    thread::sleep(duration);

    if cfg!(debug_assertions) {
        trace!("slept for #{:#?}", now.unwrap().elapsed());
    }
}
