// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use ast::ir::*;
use ast::ptr::*;
use ast::types::*;
use compiler::backend::BackendType;
use compiler::backend::RegGroup;
use runtime::thread::MuThread;
use runtime::ValueLocation;
use utils::math;
use utils::ByteSize;
use utils::ObjectReference;
use utils::*;
use vm::VM;

mod clock_posix;
mod clock_std;
// mod timer_timer_crate;
mod timer_posix;

pub mod clock;
pub mod timer;

extern "C" {
    fn call_mu_function(func_addr: Address, arg_addr: Address);
}
