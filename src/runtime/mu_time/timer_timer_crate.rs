// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate timer;
extern crate time;

use super::*;
// use self::timer::Guard;

type TimerHandler = extern "C" fn(Address) -> ();

// extern "C" {
//     fn call_mu_function(func_addr: Address, arg_addr: Address);
// }

pub struct SysTimer {
    timerref: Box<timer::Timer>,
    guard: timer::Guard,
}

impl SysTimer {
    pub(super) fn new() -> *mut SysTimer {
        trace!("SysTimer.NEW");
        let _tmr = Box::new(timer::Timer::new());
        let _tmr_ptr = Box::into_raw(Box::new(SysTimer {
            timerref: _tmr,
            guard: unsafe { std::mem::zeroed() },
        }));
        trace!("_tmr_ptr: {:#?}", _tmr_ptr);

        _tmr_ptr
    }

    pub(super) fn set(
        &mut self,
        tm: clock::TimeValT,
        func: Address,
        args: Address
    ) {
        extern crate chrono;
        trace!("SysTimer.SET ({}, {}, {})", tm, func, args);
        // let func: Arc<TimerHandler> = unsafe { Arc::from_raw(func.to_ptr_mut()) };
        // let func = func.as_usize() as *const usize as *const fn(Address);
        let _delay = time::Duration::nanoseconds(tm);
        trace!("delay: {:#?}", _delay);
        // let g = self.timerref.schedule_with_delay(_delay,  move || {
        let tmr = timer::Timer::new();
        let g = tmr.schedule_with_delay( chrono::Duration::nanoseconds(tm),  move || {
            // let g = tmr.schedule_with_delay(_delay,  move || {
            // unsafe { (*func)(args); };
            // unsafe { call_mu_function(func, args); };
            trace!("func: {:#?}", func);
            trace!("__mu_pp: {:#?}", Address::from_ref(&__mu_pp));
            unsafe { call_mu_function(func, args); };
            // unsafe { call_mu_function(Address::from_ref(&__mu_pp), args); };
            // unsafe { __mu_pp(args); };
            println!("SysTimer.SET..func.args after {:#?}", _delay);
        } );
        self.guard = g;
        trace!("SysTimer.SET returning");
    }

    pub(super) fn cancel(&mut self) {
        let g = self.guard.clone();
        drop(g);
    }
}


