// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//use super::clock_std::*;
use super::clock_posix::*;

pub type TimeValT = i64;
type TimeOffset = i64;

struct TimeInfo {
    pub forward_drift: TimeOffset
}

static mut TIMEINFO: TimeInfo = TimeInfo {
    forward_drift: 0 as TimeOffset
};

/// gets the current clock value
/// by default, it's the system clock (since UNIX_EPOCH), unless modified using
/// `muentry_settime`
#[no_mangle]
pub extern "C" fn muentry_gettime() -> TimeValT {
    sys_get_time_ns() + unsafe { TIMEINFO.forward_drift }
}

/// sets the current clock value to a new absolute time
/// `tval` is the new absolute time to be the current clock value
#[no_mangle]
pub extern "C" fn muentry_settime(tval: TimeValT) {
    unsafe {
        TIMEINFO.forward_drift = tval - sys_get_time_ns();
    };
}
