// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate rand;

use rand::Rng;

#[no_mangle]
pub extern "C" fn muentry_rand_int64(min: u64, max: u64) -> u64 {
    let mut rng = rand::thread_rng();

    let res = rng.gen_range(min, max);

    if cfg!(debug_assertions) {
        debug!("RandI({},{}) = {}", min, max, res);
    }

    res
}

#[no_mangle]
pub extern "C" fn muentry_rand_f64(min: f64, max: f64) -> f64 {
    let mut rng = rand::thread_rng();

    let res = rng.gen_range(min, max);
    if cfg!(debug_assertions) {
        debug!("RandI({},{}) = {}", min, max, res);
    }

    res
}
