// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// expose the mu interface (GC)

use ast::ir::*;
use ast::ptr::*;
use ast::types::*;
use compiler::backend::BackendType;
use compiler::backend::RegGroup;
use runtime::thread::MuThread;
use runtime::ValueLocation;
use utils::math;
use utils::ByteSize;
use utils::ObjectReference;
use utils::*;
use vm::VM;

mod mm_mu;
pub use self::mm_mu::*;

// commented for now, as the ide does not support this
//cfg_if!(
//    if #[cfg(feature="realtime")] {
//        mod mm_rtmu;
//        pub use self::mm_rtmu::*;
//    }
//)

#[cfg(feature = "realtime")]
mod mm_rtmu;

#[cfg(feature = "realtime")]
pub use self::mm_rtmu::*;

//#[cfg(all(feature = "realtime", target_os = "linux"))]
//mod mm_rtmu_linux;

// rtmu emm memory based-on rust std alloc
mod mm_rtmu_std;
