// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::RwLock;

use super::mm_rtmu_std::*;
use super::*;

lazy_static! {
    pub static ref EMM_ROOTS: RwLock<HashSet<usize>> =
        RwLock::new(HashSet::new());
    pub static ref EMM_MAP: RwLock<HashMap<usize, usize>> =
        RwLock::new(HashMap::new());
}

impl fmt::Debug for EMM_ROOTS {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "EMM_ROOTS {{ ").unwrap();
        {
            let lock = self.read().unwrap();
            for addr in lock.iter() {
                write!(f, "0x{:X}, ", addr);
            }
        }
        write!(f, "}}")
    }
}

pub fn add_to_emm_roots(backstore_addr: Address) {
    trace!("=== ADD {:?} TO_EMM_ROOTS ===", backstore_addr);
    {
        let mut emm_roots = EMM_ROOTS.write().unwrap();
        let raw_addr = backstore_addr.as_usize();
        emm_roots.insert(raw_addr);
    }
    trace!("new EMM_ROOTS: {:#?}", EMM_ROOTS);
}

pub fn delete_from_emm_roots(backstore_addr: Address) {
    trace!("=== REMOVE {:?} FROM_EMM_ROOTS ===", backstore_addr);
    {
        let mut emm_roots = EMM_ROOTS.write().unwrap();
        let raw_addr = backstore_addr.as_usize();
        emm_roots.remove(&raw_addr);
    }
    trace!("new EMM_ROOTS: {:#?}", EMM_ROOTS);
}

#[repr(C)]
pub struct RegionRootSet {
    pub refs: RwLock<HashSet<Address>>
}

impl RegionRootSet {
    pub fn new_empty() -> RegionRootSet {
        let new_lst: HashSet<Address> = HashSet::new();
        RegionRootSet {
            refs: RwLock::new(new_lst)
        }
    }
    pub fn add(&mut self, base_addr: &Address, iref_list: &Vec<ByteSize>) {
        let mut _lock = self.refs.write().unwrap();
        for iref in iref_list {
            _lock.insert(*base_addr + *iref);
        }
    }
}

use std::fmt;
impl fmt::Debug for RegionRootSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let lock = self.refs.read().unwrap();
        writeln!(f, "RegionRootSet <{} entries> {{ ", lock.len()).unwrap();
        for addr in lock.iter() {
            write!(f, "{}, ", addr);
        }
        write!(f, "}}")
    }
}

#[repr(C)]
pub struct EMMBackStore {
    // keeps track of a block separately or as backstore of a region
    pub sysbackstore: SysMemBackStore, // points to the raw memory backstore
    pub size: usize,
    pub ty_id: Option<MuID>,
    pub gcroots: Option<Box<RegionRootSet>> /* the set of offsets of irefs
                                             * in the current block or
                                             * region; this should be added
                                             * to the base-address of
                                             * the backstore to calculate
                                             * the absolute address */
}

impl fmt::Debug for EMMBackStore {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "EMMBackStore {{ ").unwrap();
        write!(f, "-raw address: {:?}, ", self.sysbackstore);
        write!(f, "-bs size: {}, ", self.size);
        writeln!(f, "-obj type: {:?}, ", self.ty_id);
        if self.gcroots.is_none() {
            writeln!(f, "-gcroots: {:?}", self.gcroots);
        } else {
            write!(f, "{:?}", self.gcroots);
        }
        write!(f, "}}")
    }
}

impl EMMBackStore {
    pub fn new(_size: usize, ty_id: Option<MuID>) -> EMMBackStore {
        trace!("=== EMMBACKSTORE.NEW ===");
        trace!("- size: {}", _size);
        trace!("- type_id: {:?}", ty_id);
        let raw_backstore = unsafe { sys_new_raw_mem(_size) };
        EMMBackStore {
            sysbackstore: raw_backstore,
            size: _size,
            ty_id: ty_id,
            gcroots: None
        }
    }
    pub fn delete(&mut self) {
        trace!("=== EMMBACKSTORE.DELETE ===");
        trace!("- {:?}", &self);
        if self.gcroots.is_some() {
            delete_from_emm_roots(Address::from_ref(&self.gcroots));
        }
        sys_delete_raw_mem(self.sysbackstore, self.size);
    }

    pub fn get_base_addr(&self) -> Address {
        sys_get_base_addr(self.sysbackstore)
    }

    pub fn has_gcroots(&self) -> bool {
        self.gcroots.is_some()
    }

    pub fn add_gcroots(&mut self, iref_list: &Vec<ByteSize>) {
        trace!("=== EMMBACKSTORE.ADD_GCROOTS ===");
        if self.gcroots.is_none() {
            self.gcroots = Some(Box::new(RegionRootSet::new_empty()));
            add_to_emm_roots(Address::from_ref(&self.gcroots));
        }
        trace!("TO ADD: {:#?}", iref_list);
        let base_addr = &self.get_base_addr();
        &self.gcroots.as_mut().unwrap().add(base_addr, iref_list);
        trace!("- new gcroots content: {:?}", &self.gcroots);
    }
}

#[repr(C)]
pub struct EMMRegion {
    pub emmbackstore: Box<EMMBackStore>,
    pub size: usize,
    pub freeptr: Address
}

impl fmt::Debug for EMMRegion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "EMMRegion {{ ").unwrap();
        write!(f, "-size: {}, ", self.size);
        writeln!(f, "-starting address= {}", self.freeptr);
        writeln!(f, "- {:?}", self.emmbackstore);
        write!(f, "}}")
    }
}

impl EMMRegion {
    /// Allocates the backstore memory for a region, and creates and initializes
    /// a new `EEMRegion` struct too keep track of it.
    ///
    /// # Arguments
    ///
    /// * `_size` - An integer representing the region size in bytes
    ///
    /// # Returns
    ///
    /// * A new object of type `EMMRegion`, as explained above.
    pub fn new(_size: usize) -> EMMRegion {
        let size = sys_get_aligned_size(_size, POINTER_SIZE);
        let emm_backstore = Box::new(EMMBackStore::new(size, None));
        let free_ptr = emm_backstore.sysbackstore;
        let emm_region = unsafe {
            EMMRegion {
                emmbackstore: emm_backstore,
                size: _size,
                freeptr: Address::from_mut_ptr(free_ptr)
            }
        };

        emm_region
    }

    /// Deletes the backstore memory allocated as part of `new(_size)` method.
    ///
    /// Note that the `EMMRegion` object itself should be deleted separately.
    pub fn delete_backstore(&mut self) {
        self.emmbackstore.delete(); // delete the raw system memory first
                                    //        unsafe {
                                    //
                                    // Box::from_raw(self.emmbackstore);  //drop
                                    // the EMMbacktore
                                    // of the region        }
    }

    /// Allocates `_size` number of bytes inside the current region.
    ///
    /// Note that allocation inside a region does not support `delete`,
    /// so we only keep the free space head.
    ///
    /// # Returns
    ///
    /// * If successful, it returns the `Address` to the beginning of the newly
    ///   allocated space,
    /// and `Address(null)` otherwise.
    pub fn ralloc(&mut self, _size: usize) -> Address {
        let _size = sys_get_aligned_size(_size, POINTER_SIZE);
        let res = self.freeptr;
        if self.has_enough_space(_size) {
            trace!("RALLOC: allocating {} bytes", _size);
            self.freeptr += _size as ByteOffset;
            res
        } else {
            trace!("RALLOC: not enough free space, returning NULL");
            unsafe { Address::from_usize(0) }
        }
    }

    pub fn get_base_addr(&self) -> Address {
        self.emmbackstore.get_base_addr()
    }

    pub fn has_gcroots(&self) -> bool {
        self.emmbackstore.has_gcroots()
    }

    pub fn add_gcroots(&mut self, iref_list: &Vec<ByteSize>) {
        self.emmbackstore.add_gcroots(iref_list);
    }

    // checks whether there is enough space to allocate a new block of `_size`
    // length
    fn has_enough_space(&self, _size: usize) -> bool {
        let limit = unsafe { self.get_base_addr() + self.size };

        (self.freeptr + _size) <= limit
    }
}

#[no_mangle]
pub extern "C" fn muentry_new_reg(size: usize) -> Address {
    trace!("=== NEW REGION ===");
    let new_region = Box::new(EMMRegion::new(size));
    trace!("- {:?}", new_region);
    Address::from_ptr(Box::into_raw(new_region))
}

#[no_mangle]
pub extern "C" fn muentry_delete_reg(regionref: Address) {
    trace!("=== DELETE REGION ===");
    let mut regionbox =
        unsafe { Box::from_raw(regionref.to_ptr_mut() as *mut EMMRegion) };
    regionbox.delete_backstore();
}

/// Given the Address of a region struct `EMMRegion`,
/// allocates a space of `size` length
/// (may allocate more to preserve alignment)
///
/// args: `regionref` is a mutable pointer to the `EMMRegion` struct of the
/// target region args: `size` is the length of the required space in bytes
///
/// returns: an `Address` struct, which is a pointer to the newly allocated
/// space
#[no_mangle]
pub extern "C" fn muentry_ralloc(regionref: Address, size: usize) -> Address {
    trace!("=== RALLOC ===");
    trace!("- size: -{}", size);
    let mut emmregion = unsafe { regionref.to_ptr_mut() as *mut EMMRegion };
    trace!("- from region: {:?}", unsafe { &(*emmregion) });
    unsafe { (*emmregion).ralloc(size) }
}

#[no_mangle]
pub extern "C" fn muentry_ralloc_hybrid(
    regionref: Address,
    fixed_len: usize,
    var_unit_len: usize,
    var_len: usize
) -> Address {
    trace!("=== RALLOCHYBRID ===");
    let size = fixed_len + (var_len * var_unit_len);
    trace!("- size: -{}", size);
    let mut emmregion = unsafe { regionref.to_ptr_mut() as *mut EMMRegion };
    trace!("- from region: {:?}", unsafe { &(*emmregion) });
    unsafe { (*emmregion).ralloc(size) }
}

#[no_mangle]
pub extern "C" fn muentry_ralloc_traced(
    regionref: Address,
    _type: MuID
) -> Address {
    trace!("=== RALLOC TRACED===");

    let vm = MuThread::current_mut().vm.clone();
    trace!("found Arc<VM>");
    let ty_info = vm.get_backend_type_info(_type);
    trace!("- obj backend type: -{:#?}", ty_info);
    let size = ty_info.size;
    trace!("- obj size: -{}", size);

    let mut emmregion = unsafe { regionref.to_ptr_mut() as *mut EMMRegion };
    trace!("- from region: {:?}", unsafe { &(*emmregion) });
    let sub_reg_addr = unsafe { (*emmregion).ralloc(size) };

    let iref_list = ty_info.get_iref_offsets(&vm, None);
    trace!("- adding to region roots: {:?}", &iref_list);
    unsafe {
        (*emmregion).add_gcroots(&iref_list);
    };

    sub_reg_addr
}

#[no_mangle]
pub extern "C" fn muentry_ralloc_hybrid_traced(
    regionref: Address,
    _type: MuID,
    var_len: usize
) -> Address {
    trace!("=== RALLOCHYBRID TRACED===");

    let vm = MuThread::current_mut().vm.clone();
    let ty_info = vm.get_backend_type_info(_type);
    trace!("- obj backend type: -{:#?}", ty_info);
    let size = ty_info.size + (ty_info.elem_size.as_ref().unwrap() * var_len);
    trace!("- total obj size: -{}", size);

    let mut emmregion = unsafe { regionref.to_ptr_mut() as *mut EMMRegion };
    trace!("- from region: {:?}", unsafe { &(*emmregion) });
    let sub_reg_addr = unsafe { (*emmregion).ralloc(size) };

    let iref_list = ty_info.get_iref_offsets(&vm, Some(var_len));
    trace!("- adding to region roots: {:?}", &iref_list);
    unsafe {
        (*emmregion).add_gcroots(&iref_list);
    };

    sub_reg_addr
}

#[no_mangle]
pub extern "C" fn muentry_ealloc(size: usize) -> Address {
    let emm_backstore = Box::new(EMMBackStore::new(size, None));
    let raw_ptr = Address::from_mut_ptr(emm_backstore.sysbackstore);
    let bs_ptr = Address::from_mut_ptr(Box::into_raw(emm_backstore));

    let mut map = EMM_MAP.write().unwrap();
    map.insert(raw_ptr.as_usize(), bs_ptr.as_usize());

    raw_ptr
}

#[no_mangle]
pub extern "C" fn muentry_ealloc_hybrid(
    fixed_len: usize,
    var_unit_len: usize,
    var_len: usize
) -> Address {
    let size = fixed_len + var_len * var_unit_len;
    trace!("=== EALLOCHYBRID size #{}===", size);
    let emm_backstore = Box::new(EMMBackStore::new(size, None));
    let raw_ptr = Address::from_mut_ptr(emm_backstore.sysbackstore);
    let bs_ptr = Address::from_mut_ptr(Box::into_raw(emm_backstore));

    let mut map = EMM_MAP.write().unwrap();
    map.insert(raw_ptr.as_usize(), bs_ptr.as_usize());

    raw_ptr
}

#[no_mangle]
pub extern "C" fn muentry_edelete(raw_ptr: Address) {
    let map = EMM_MAP.read().unwrap();
    let bs_ptr =
        unsafe { Address::from_usize(*map.get(&raw_ptr.as_usize()).unwrap()) };

    let mut emm_backstore: Box<EMMBackStore> =
        unsafe { Box::from_raw(bs_ptr.to_ptr_mut() as *mut EMMBackStore) };
    emm_backstore.delete()
}

#[no_mangle]
pub extern "C" fn muentry_ealloc_traced(_type: MuID) -> Address {
    let vm = MuThread::current_mut().vm.clone();
    let ty_info = vm.get_backend_type_info(_type);
    let size = ty_info.size;

    let mut emm_backstore = Box::new(EMMBackStore::new(size, Some(_type)));

    let iref_list = ty_info.get_iref_offsets(&vm, None);
    emm_backstore.add_gcroots(&iref_list);

    let raw_ptr = Address::from_mut_ptr(emm_backstore.sysbackstore);
    let bs_ptr = Address::from_mut_ptr(Box::into_raw(emm_backstore));

    let mut map = EMM_MAP.write().unwrap();
    map.insert(raw_ptr.as_usize(), bs_ptr.as_usize());

    raw_ptr
}

#[no_mangle]
pub extern "C" fn muentry_ealloc_hybrid_traced(
    _type: MuID,
    var_len: usize
) -> Address {
    let vm = MuThread::current_mut().vm.clone();
    let ty_info = vm.get_backend_type_info(_type);
    let size = ty_info.size + (ty_info.elem_size.as_ref().unwrap() * var_len);

    let mut emm_backstore = Box::new(EMMBackStore::new(size, Some(_type)));

    let iref_list = ty_info.get_iref_offsets(&vm, Some(var_len));
    emm_backstore.add_gcroots(&iref_list);

    let raw_ptr = Address::from_mut_ptr(emm_backstore.sysbackstore);
    let bs_ptr = Address::from_mut_ptr(Box::into_raw(emm_backstore));

    let mut map = EMM_MAP.write().unwrap();
    map.insert(raw_ptr.as_usize(), bs_ptr.as_usize());

    raw_ptr
}

#[no_mangle]
pub extern "C" fn muentry_edelete_traced(backstore: Address) {
    muentry_edelete(backstore);
}
