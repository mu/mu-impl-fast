// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifdef __linux__
// RTLD_DEFAULT is not defined in POSIX. Linux gcc does not define it unless
// _GNU_SOURCE is also defined.
#define _GNU_SOURCE
#define _POSIX_C_SOURCE
#endif // __linux__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

uint32_t mu_retval;

__thread void *mu_tls;

void muentry_set_retval(uint32_t x)
{
    mu_retval = x;
}

int32_t c_check_result()
{
    return mu_retval;
}

char *alloc_mem(size_t size)
{
    return (char *)malloc(size);
}

void set_thread_local(void *thread)
{
    // printf("Thread%p: setting mu_tls to %p\n", (void*) pthread_self(), thread);
    mu_tls = thread;
}

void *muentry_get_thread_local()
{
    //    printf("Thread%p: getting mu_tls as %p\n", (void*) pthread_self(), mu_tls);
    return mu_tls;
}

void *resolve_symbol(const char *sym)
{
    // printf("%s\n", sym);
    return dlsym(RTLD_DEFAULT, sym);
}

void call_mu_function(int64_t func_addr, int64_t arg_addr)
{
    void (*func)(void *) = func_addr;
    (*func)(arg_addr);
    // __asm__ volatile (
    //     "push %%rdi\n"
    //     "push %%r10\n"
    //     "movq %[arg], %%rdi\n"
    //     "movq %[func], %%r10\n"
    //     "call *%%r10\n"
    //     "pop %%r10\n"
    //     "pop %%rdi\n"
    //     :   // no output
    //     : [func] "rm" (func_addr),
    //     [arg] "rm" (arg_addr)
    //     : "rdi", "r10"
    // );
}

/**
 * C code to handler timer events goes here
 */

#define errExit(msg)        \
    do                      \
    {                       \
        perror(msg);        \
        exit(EXIT_FAILURE); \
    } while (0)

#define TMRSIG SIGRTMIN

struct _TmrHandler
{
    /* data */
    void *func_ptr;
    void *arg_ptr;
};
typedef struct _TmrHandler TmrHandler;

typedef void (*MuFunc)(void *);

static void
handler(int sig, siginfo_t *si, void *uc)
{
    TmrHandler *h = (TmrHandler *)si->si_value
                        .sival_ptr;
    MuFunc func = (MuFunc)h->func_ptr;
    func(h->arg_ptr);
}

/**
 * Given a timer handler spec, this function initializes a timer object and
 * return a pointer to it.
 *
 * Note: The timer handler spec is always located at the fixed address pointed
 * to be `_tmr_handler`, but the internal content needs to be updated by the
 * `timer_settime` function
 *
 * @param _tmr_handler is a pointer to a previously allocated object which
 * includes the necessary memory space to keep track of the timer handler spec.
 * @return a pointer to the newly allocated timer object
 */
void *posix_timer_create(void *_tmr_handler)
{
    struct sigevent sev;
    sigset_t mask;
    struct sigaction sa;

    timer_t *timer_id = (timer_t *)malloc(sizeof(timer_t));

    /* Establish handler for timer signal */

    printf("Establishing handler for signal %d\n", TMRSIG);
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(TMRSIG, &sa, NULL) == -1)
        errExit("sigaction");

    /* Block timer signal temporarily */

    printf("Blocking signal %d\n", TMRSIG);
    sigemptyset(&mask);
    sigaddset(&mask, TMRSIG);
    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
        errExit("sigprocmask");

    /* Create the timer */

    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = TMRSIG;
    sev.sigev_value
        .sival_ptr = _tmr_handler;
    if (timer_create(CLOCK_REALTIME, &sev, timer_id) == -1)
        errExit("timer_create");

    printf("Unblocking signal %d\n", TMRSIG);
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
        errExit("sigprocmask");

    printf("TMR_CREATE timer ID is 0x%lx\n", (long)timer_id);

    return (void *)timer_id;
}

void posix_timer_settime(void *timer_ptr, uint64_t dur, uint64_t prd)
{
    struct itimerspec its;

    printf("TMR_SETTIME (0x%lx, %lx, %lx)\n", (long)timer_ptr, dur, prd);

    its.it_value
        .tv_sec = dur / 1000000000;
    its.it_value
        .tv_nsec = dur % 1000000000;
    its.it_interval
        .tv_sec = prd / 1000000000;
    its.it_interval
        .tv_nsec = prd % 1000000000;

    if (timer_settime(*((timer_t*)timer_ptr), 0, &its, NULL) == -1)
        errExit("timer_settime_arm");
}

void posix_timer_cancel(void *timer_ptr)
{
    struct itimerspec its;

    its.it_value
        .tv_sec = 0;
    its.it_value
        .tv_nsec = 0;
    its.it_interval
        .tv_sec = 0;
    its.it_interval
        .tv_nsec = 0;

    if (timer_settime(*((timer_t*)timer_ptr), 0, &its, NULL) == -1)
        errExit("timer_settime_disarm");
}

void posix_timer_delete(void *timer_ptr)
{
    timer_t t = *((timer_t *)timer_ptr);
    timer_delete(t);
    free(timer_ptr);
}